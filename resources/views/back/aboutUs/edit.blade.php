@extends('layouts.default')

@section('content')
    
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-5">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			
		</div>
	</div>
</div>
<div class="page-inner mt--5">
	<div class="row">
		<div class="col-md-12">
			<div class="card full-height">
				<div class="card-header">
					<div class="card-head-row">
						<div class="card-title">Edit <i>{{ $aboutus->subtitle }}</i></div>
                        <a href="{{ route ('aboutus.index') }}" class="btn btn-warning ml-auto"> <i class="fa fa-undo"></i> Back</a>
					</div>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ route('aboutus.update', $aboutus->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{ $aboutus->title }}" class="form-control" id="title" placeholder="Masukkan Title About Us">
                        </div>
                        <div class="form-group">
                            <label for="subtitle">Subtitle</label>
                            <input type="text" name="subtitle" value="{{ $aboutus->subtitle }}" class="form-control" id="subtitle" placeholder="Masukkan Subtitle About Us">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Konten</label>
                            <textarea name="deskripsi" class="form-control" id="deskripsi" value="{{ $aboutus->deskripsi }}">{{ $aboutus->deskripsi }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="logo">Logo  </label>
                            <input type="file" name="logo" class="form-control">
                            <br>
                            <label for="logo">Logo Saat ini </label> <br>
                            <img src="{{ asset('uploads/'. $aboutus->logo) }}" alt="" width="150">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>

@endsection