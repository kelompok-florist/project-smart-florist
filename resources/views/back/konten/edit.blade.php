@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">

        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Edit konten {{ $konten->title }}</div>
                        <a href="{{ route ('konten.index') }}" class="btn btn-warning ml-auto">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('konten.update',$konten->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Title Konten</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ $konten->title }}">
                        </div>
                        <div class="form-group">
                            <label for="subtitle">Subtitle Konten</label>
                            <input type="text" name="subtitle" class="form-control" id="subtitle" value="{{ $konten->subtitle }}">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Konten</label>
                            <textarea name="deskripsi" class="form-control" id="editor" value="{{ $konten->deskripsi }}">{{ $konten->deskripsi }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar </label>
                            <input type="file" name="gambar" class="form-control">
                            <br>
                            <label for="gambar">Gambar Saat ini </label> <br>
                            <img src="{{ asset('uploads/'. $konten->gambar) }}" alt="" width="150">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                            <button class="btn btn-danger " type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor' );
</script>
@endsection