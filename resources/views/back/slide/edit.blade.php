@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">

        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Edit Slider {{ $slide->judul_slide }}</div>
                        <a href="{{ route ('slide.index') }}" class="btn btn-warning ml-auto">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('slide.update',$slide->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="judul_slide">Judul Slider</label>
                            <input type="text" name="judul_slide" class="form-control" id="judul_slide" value="{{ $slide->judul_slide }}">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" id="editor" cols="30" rows="10">{{ $slide->deskripsi }}"</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nowa">Nomor WhatsApp</label>
                            <input type="text" name="nowa" class="form-control" id="nowa" value="{{ $slide->nowa }}">
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar Slider </label>
                            <input type="file" name="gambar_slide" class="form-control">
                            <br>
                            <label for="gambar">Gambar Saat ini </label> <br>
                            <img src="{{ asset('uploads/'. $slide->gambar_slide) }}" alt="" width="150">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                            <button class="btn btn-danger " type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
</script>
@endsection