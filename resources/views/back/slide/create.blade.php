@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">

        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Form Slider</div>
                        <a href="{{ route ('slide.index') }}" class="btn btn-warning ml-auto">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                        @endforeach
                    </div>
                    @endif
                    <form method="POST" action="{{ route('slide.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="judul_slide">Judul Slider</label>
                            <input type="text" name="judul_slide" class="form-control" id="judul_slide" placeholder="Masukkan Judul Slider">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <textarea name="deskripsi" id="editor" cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="nowa">Nomor WhatsApp</label>
                            <input type="number" id="nowa" name="nowa" class="form-control" placeholder="Masukkan Nomor Dimulai dari 8 Slider">
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar </label>
                            <input type="file" name="gambar_slide" id="gambar" class="form-control">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                            <button class="btn btn-danger " type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
</script>

@endsection