@extends('layouts.default')

@section('content')
    
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-5">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			
		</div>
	</div>
</div>
<div class="page-inner mt--5">
	<div class="row">
		<div class="col-md-12">
			<div class="card full-height">
				<div class="card-header">
					<div class="card-head-row">
						<div class="card-title">Edit Kategori  <i>{{ $kategori->nama_kategori }}</i></div>
                        <a href="{{ route ('kategori.index') }}" class="btn btn-warning ml-auto"> <i class="fa fa-undo"></i> Back</a>
					</div>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ route('kategori.update', $kategori->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="kategori">Nama Kategori</label>
                            <input type="text" name="nama_kategori" value="{{ $kategori->nama_kategori }}" class="form-control" id="text" placeholder="Masukkan Nama Kategori">
                        </div>
						<div class="form-group">
                            <label for="gambar kategori">gambar kategori  </label>
                            <input type="file" name="gambar kategori" class="form-control">
                            <br>
                            <label for="gambar kategori">gambar kategori Saat ini </label> <br>
                            <img src="{{ asset('uploads/'. $kategori->gambar_kategori) }}" alt="" width="150">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection