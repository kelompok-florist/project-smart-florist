@extends('layouts.default')

@section('content')
    
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-5">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			
		</div>
	</div>
</div>
<div class="page-inner mt--5">
	<div class="row">
		<div class="col-md-12">
			<div class="card full-height">
				<div class="card-header">
					<div class="card-head-row">
						<div class="card-title">Edit footer</div>
                        <a href="{{ route ('footer.index') }}" class="btn btn-warning ml-auto"> <i class="fa fa-undo"></i> Back</a>
					</div>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ route('footer.update', $footer->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="footer">footer</label>
                            <input type="text" name="footer" id="footer" value="{{ $footer->footer }}" class="form-control" id="footer" placeholder="Masukkan Isi Footer">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection