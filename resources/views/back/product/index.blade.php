@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Data Produk</div>
                        <a href="{{ route ('product.create') }}" class="btn btn-primary ml-auto"> <i class="fas fa-plus"></i> Tambah Produk</a>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('success'))
                    <div class="alert alert-primary">
                        {{ Session('success') }}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table id="example1" class="table ">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama Produk</th>
                                    <th>Kategori</th>
                                    <th>Harga</th>
                                    <th>No Wa</th>
                                    <th>Provinsi</th>
                                    <th>Kabupaten</th>
                                    <th>Status</th>
                                    <th>Gambar</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($product as $key => $row)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $row -> nama }}</td>
                                    <td>{{ $row -> kategori->nama_kategori }}</td>
                                    <td>{{ $row -> harga }}</td>
                                    <td>{{ $row -> nowa }}</td>
                                    <td>{{ $row -> province->name }}</td>
                                    <td>{{ $row -> regency->name }}</td>
                                    <td>
                                        @if ($row->bestseller == '1')
                                        bestseller
                                        @else
                                        regular
                                        @endif
                                    </td>

                                    <td>
                                        @if ($row->gambars && count($row->gambars) > 0)
                                        {{-- @foreach ($row->gambars as $data) --}}
                                        <img src="{{ asset( $row->gambars[0]->gambar) }}" alt="img" style="width: 80px; height:90px" class="me-4">
                                        @else
                                        <h5>No Image Uploaded</h5>
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{ route ('product.edit', $row->id) }}" class="btn btn-warning btn-sm mb-2"><i class="fas fa-pencil"></i> </a>

                                        <form action="{{ route ('product.destroy', $row->id) }}" method="POST" class="d-inline">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger btn-sm mb-2">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="8" class="text-center">No Product Available</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection