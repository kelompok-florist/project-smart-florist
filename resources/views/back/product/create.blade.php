@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">

        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Form Product</div>
                        <a href="{{ route ('product.index') }}" class="btn btn-warning ml-auto">Back</a>
                    </div>
                </div>
                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                        @endforeach
                    </div>
                    @endif

                    <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                        @csrf
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                              <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
                            </li>
                            <li class="nav-item" role="presentation">
                              <a class="nav-link" id="pills-deskripsi-tab" data-toggle="pill" href="#pills-deskripsi" role="tab" aria-controls="pills-deskripsi" aria-selected="false">Deskripsi</a>
                            </li>
                            <li class="nav-item" role="presentation">
                              <a class="nav-link" id="pills-gambar-tab" data-toggle="pill" href="#pills-gambar" role="tab" aria-controls="pills-gambar" aria-selected="false">Gambar</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan nama product">
                                </div>
                                
                                
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <input type="number" name="harga" class="form-control" id="harga" placeholder="Masukkan harga product">
                                </div>
                               

                                <style>
                                    .readonly-wrapper {
                                        position: relative;
                                        display: inline-block;
                                    }
        
                                    .readonly-text {
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        background-color: #f0f0f0;
                                        pointer-events: none;
                                    }
                                </style>
                                 <div class="form-group">
                                    <label for="nowa">No Wa</label>
                                    <div class="input-group">
                                        <div class="col-md-1">
                                            <div class="readonly-wrapper">
                                                <input type="number" name="nowa" value="62" class="form-control">
                                            </div>
                                        </div>
                                        <div class=" col-md-11">
                                            <input type="tel" name="nowa" class="form-control" id="nowa" placeholder="Masukkan No Wa Mulai Dari 8">
                                        </div>
                                    </div>
                                    
                                </div>
        
                                <div class="form-group">
                                    <label for="kategori_id">Kategori </label>
                                    <select name="kategori_id" class="form-control" id="kategori_id" data-placeholder="asdasd">
                                        <option>Pilih Kategori...</option>
                                        @foreach ($kategori as $row)
                                        <option value="{{ $row->id }}">{{ $row->nama_kategori }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                
        
                                <div class="form-group">
                                    <label for="provinces">Provinsi </label>
                                    <select name="provinces_id" class="form-control" id="provinces">
                                        <option>Pilih Provinsi..</option>
                                        @foreach ($provinces as $row)
                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
        
                                <div class="form-group">
                                    <label for="kabupaten">Kabupaten </label>
                                    <select name="regencies_id" id="kabupaten" class="form-control">
                                        <option>Pilih Kabupaten..</option>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="bestseller">Status</label>
                                    <select name="bestseller" class="form-control">
                                        <option value="">Pilih Status Product..</option>
                                        <option value="1">bestseller</option>
                                        <option value="0">regular</option>
                                    </select>
                                </div>
                                
                            </div>

                            <div class="tab-pane fade" id="pills-deskripsi" role="tabpanel" aria-labelledby="pills-deskripsi-tab">
                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi</label>
                                    <textarea name="deskripsi" id="editor" class="form-control"> </textarea>
                                </div>
                               
                            </div>
                            <div class="tab-pane fade" id="pills-gambar" role="tabpanel" aria-labelledby="pills-gambar-tab">
                                <div class="form-group">
                                    <label for="gambar">Gambar</label>
                                    <input type="file" id="gambar" name="gambar[]" class="form-control" multiple>
                                </div>
                                
                                <div class="form-group">
                                    <button class="btn btn-primary " type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor');
</script>

<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#provinces').on('change', function() {
            let id_provinsi = $('#provinces').val();

            $.ajax({
                type: 'POST',
                url: "{{ route('getKabupaten') }}",
                data: {
                    id_provinsi: id_provinsi
                },
                cache: false,

                success: function(msg) {
                    $('#kabupaten').html(msg);
                },
                error: function(data) {
                    console.log('error:', data)
                },
            })
        });
    });
</script>

@endsection