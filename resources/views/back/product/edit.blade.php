@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">

        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Edit Produk {{ $product->nama }}</div>
                        <a href="{{ route ('product.index') }}" class="btn btn-warning ml-auto">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('product.update',$product->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')  
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
                            </li>
                            <li class="nav-item" role="presentation">
                            <a class="nav-link" id="pills-deskripsi-tab" data-toggle="pill" href="#pills-deskripsi" role="tab" aria-controls="pills-deskripsi" aria-selected="false">Deskripsi</a>
                            </li>
                            <li class="nav-item" role="presentation">
                            <a class="nav-link" id="pills-gambar-tab" data-toggle="pill" href="#pills-gambar" role="tab" aria-controls="pills-gambar" aria-selected="false">Gambar</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan nama product" value="{{ $product->nama }}">
                                </div>
                                @error('nama')
                                <div class="alert alert-primary">{{ $message }}</div>
                                @enderror
                                
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <input type="number" name="harga" class="form-control" id="harga" placeholder="Masukkan harga product" value="{{ $product->harga }}">
                                </div>
                                @error('harga')
                                <div class="alert alert-primary">{{ $message }}</div>
                                @enderror

                                <style>
                                    .readonly-wrapper {
                                        position: relative;
                                        display: inline-block;
                                    }
        
                                    .readonly-text {
                                        display: inline-block;
                                        border: 1px solid #ccc;
                                        background-color: #f0f0f0;
                                        pointer-events: none;
                                    }
                                </style>
                               <div class="form-group">
                                <label for="nowa">No Wa</label>
                                <div class="input-group">
                                    <div class="col-md-1">
                                        <div class="readonly-wrapper">
                                            <input type="number" name="nowa" value="62" class="form-control">
                                        </div>
                                    </div>
                                    <div class=" col-md-11">
                                        <?php $nowaValue = $product->nowa ?? ''; ?>
                                        <input type="tel" name="nowa" class="form-control" id="nowa" placeholder="Masukkan No Wa Mulai Dari 8" value="{{ substr($nowaValue, 2) }}">
                                    </div>
                                </div>
                                @error('nowa')
                                <div class="alert alert-primary">{{ $message }}</div>
                                @enderror
                            </div>
                            
        
                                <div class="form-group">
                                    <label for="kategori_id">Kategori </label>
                                    <select name="kategori_id" id="kategori" class="form-control">
                                        @foreach ($kategori as $row)
                                        @if ($row->id == $product->kategori_id)
                                        <option value={{ $row->id }} selected='selected'>{{ $row->nama_kategori }}</option>
                                        @else
                                        <option value="{{ $row->id }}">{{ $row->nama_kategori }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                @error('kategori_id')
                                <div class="alert alert-primary">{{ $message }}</div>
                                @enderror
        
                                <div class="form-group">
                                    <label for="provinces">Provinsi </label>
                                    <select name="provinces_id"  class="form-control" id="provinces">
                                        @foreach ($provinces as $row)
                                        @if ($row->id == $product->provinces_id)
                                        <option value={{ $row->id }} selected='selected'>{{ $row->name }}</option>
                                        @else
                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
        
                                <div class="form-group">
                                    <label for="kabupaten">Kabupaten </label>
                                    <select name="regencies_id" id="kabupaten" class="form-control">
                                        <option>Pilih Kabupaten..</option>
                                    </select>
                                </div>

                                {{-- <div class="form-group">
                                    <label for="bestseller">bestseller </label>
                                    <input type="checkbox" id="bestseller" name="bestseller" {{ $product->bestseller == '1' ? 'checked':'' }} style="width: 30px; height:30px; margin: 3px">
                                </div> --}}

                                <div class="form-group">
                                    <label for="bestseller">bestseller</label>
                                    <select name="bestseller" class="form-control">
                                        <option value="1" {{ $product->bestseller == '1' ? 'selected' : '' }}>bestseller</option>
                                        <option value="0" {{ $product->bestseller == '0' ? 'selected' : '' }}>regular</option>
                                    </select>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pills-deskripsi" role="tabpanel" aria-labelledby="pills-deskripsi-tab">
                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi</label>
                                    <textarea name="deskripsi" id="editor" class="form-control">{{ $product->deskripsi}} </textarea>
                                </div>
                                @error('deskripsi')
                                <div class="alert alert-primary">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="tab-pane fade" id="pills-gambar" role="tabpanel" aria-labelledby="pills-gambar-tab">
                                <div class="form-group">
                                    <label for="gambar">Gambar</label>
                                    <input type="file" id="gambar" name="gambar[]" class="form-control" multiple>
                                </div>
                                <div>
                                    @if ($product->gambars)
                                    <div class="row">
                                        @foreach ($product->gambars as $gambar)
                                        <div class="col-md-2">
                                            <img src="{{ asset( $gambar->gambar) }}" alt="img" style="width: 80px; height:90px" class="m-2 border">
                                            <a href="{{ url('product/product_image/'.$gambar->id.'/delete') }}" class="d-block">Remove</a>
                                        </div>
                                        @endforeach
                                    </div>  

                                    @else
                                    <h5>No Image Uploaded</h5>
                                    @endif
                                </div>
                                @error('gambar')
                                <div class="alert alert-primary">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <button class="btn btn-primary " type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Fungsi untuk mengambil kabupaten berdasarkan provinsi
        function ambilKabupaten(id_provinsi) {
            $.ajax({
                type: 'POST',
                url: "{{ route('getKabupaten') }}",
                data: {
                    id_provinsi: id_provinsi
                },
                cache: false,
                success: function (msg) {
                    $('#kabupaten').html(msg);
                },
                error: function (data) {
                    console.log('error:', data);
                },
            });
        }

        // Memanggil fungsi pada saat halaman dimuat
        let idProvinsiAwal = "{{ $product->provinces_id }}";
        ambilKabupaten(idProvinsiAwal);

        // Memanggil fungsi ketika dropdown provinsi berubah
        $('#provinces').on('change', function () {
            let idProvinsi = $(this).val();
            ambilKabupaten(idProvinsi);
        });
    });
</script>


@endsection