@extends('layouts.default')

@section('content')
    
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-5">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			
		</div>
	</div>
</div>
<div class="page-inner mt--5">
	<div class="row">
		<div class="col-md-12">
			<div class="card full-height">
				<div class="card-header">
					<div class="card-head-row">
						<div class="card-title">Edit contact</div>
                        <a href="{{ route ('contact.index') }}" class="btn btn-warning ml-auto"> <i class="fa fa-undo"></i> Back</a>
					</div>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ route('contact.update', $contact->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" name="alamat" id="alamat" value="{{ $contact->alamat }}" class="form-control" id="alamat" placeholder="Masukkan Alamat">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="number" name="phone" id="phone" value="{{ $contact->phone }}" class="form-control" id="phone" placeholder="Masukkan No Handphone" maxlength="13">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" value="{{ $contact->email }}" class="form-control" id="email" placeholder="Masukkan Alamat email">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection