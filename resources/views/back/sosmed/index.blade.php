@extends('layouts.default')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">

        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-head-row">
                        <div class="card-title">Data Sosmed</div>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('success'))
                    <div class="alert alert-primary">
                        {{ Session('success') }}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama Website</th>
                                    <th>WhatsApp</th>
                                    <th>Email Sales</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($sosmed as $key => $row)
                                <tr>
                                    <td>{{ $key + 1}}</td>
                                    <td>{{ $row -> nama_website }}</td>
                                    <td>{{ $row -> whatsapp }}</td>
                                    <td>{{ $row -> email_sales }}</td>
                                    <td>
                                        <a href="{{ route ('sosmed.edit', $row->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pencil"></i> </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="8" class="text-center" >Data Masih Kosong</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection