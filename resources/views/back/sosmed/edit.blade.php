@extends('layouts.default')

@section('content')
    
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-5">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			
		</div>
	</div>
</div>
<div class="page-inner mt--5">
	<div class="row">
		<div class="col-md-12">
			<div class="card full-height">
				<div class="card-header">
					<div class="card-head-row">
						<div class="card-title">Edit sosmed</div>
                        <a href="{{ route ('sosmed.index') }}" class="btn btn-warning ml-auto"> <i class="fa fa-undo"></i> Back</a>
					</div>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ route('sosmed.update', $sosmed->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama_website">Nama Website</label>
                            <input type="text" name="nama_website" id="nama_website" value="{{ $sosmed->nama_website }}" class="form-control" id="text" placeholder="Masukkan Nama Website">
                        </div>
                        <div class="form-group">
                            <label for="whatsapp">whatsapp</label>
                            <input type="number" name="whatsapp" id="whatsapp" value="{{ $sosmed->whatsapp }}" class="form-control" id="text" placeholder="Masukkan Nomor WhatsApp" maxlength="13">
                        </div>
                        <div class="form-group">
                            <label for="email_sales">Email Sales</label>
                            <input type="email" name="email_sales" id="email_sales" value="{{ $sosmed->email_sales }}" class="form-control" id="text" placeholder="Masukkan Nama email_sales">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary " type="submit">Save</button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection