<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Smart-Florist</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" type="image/x-icon" href="{{asset('template')}}/images/logo/logow.png" />

	<!-- Fonts and icons -->
	<script src="{{ asset('back/js/plugin/webfont/webfont.min.js') }}"></script>
	<script src="https://kit.fontawesome.com/d0c4d0fd46.js" crossorigin="anonymous"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Lato:300,400,700,900"]
			},
			custom: {
				"families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
				urls: ['/back/css/fonts.min.css']
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('back/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('back/css/atlantis.min.css') }}">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{ asset('back/css/demo.css') }}">

</head>

<body data-background-color="bg3">
	<div class="wrapper">

		@include('includes.header')
		<!-- Sidebar -->
		@include('includes.sidebar')
		<!-- End Sidebar -->
		<div class="main-panel">

			<div class="content">

				@yield('content')

			</div>

			@include('includes.footer')

		</div>


	</div>
	@include('includes.js')
	@include('sweetalert::alert')
</body>

</html>