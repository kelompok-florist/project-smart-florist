<!--Footer Area Start-->
<footer class="footer-area ">
    <div class="footer-widget-area">
        <div class="container container-default center-area">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-custom">
                    <div class="single-footer-widget m-0">
                        @foreach ($aboutus as $item)
                        <div class="footer-logo">
                            <a href="/">
                                <img src="{{ asset('uploads/'. $item->logo) }}" alt="Logo Image" style="max-width: 100%; height: 35mm">
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
                @foreach ($aboutus as $item)
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-custom">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">{{ $item->subtitle }}</h2>
                        <ul class="widget-list">
                            <p>
                                {!! $item->deskripsi !!}
                            </p>
                        </ul>
                    </div>
                </div>
                @endforeach
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-custom" style="padding-right: 0px;padding-left:0;">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">Contact</h2>
                        <div class="widget-body">
                            @foreach ($contact as $item)
                            <address>
                                <strong>Alamat. </strong><a href="https://www.google.com/maps/search/?api=1&query={{ $item->alamat }}">{{ $item->alamat }}</a> <br>
                                <strong>Phone. </strong><a href="tel://{{ $item->phone }}">{{ $item->phone }}</a><br>
                                <strong>Email. </strong><a href="mailto:{{ $item->email }}">{{ $item->email }}</a><br>
                            </address>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-custom">
                    <div class="single-footer-widget">
                        <h2 class="widget-title">Follow us</h2>
                        @foreach ($sosmed as $item)
                        <div class="widget-body">
                            <address>
                                <strong><a href="/">{{ $item->nama_website }}</a></strong><br>
                                <strong>WhatsApp. </strong><a href="tel://{{ $item->whatsapp }}">{{ $item->whatsapp }}</a><br>
                                <strong> Email. <a href="mailto:{{ $item->email_sales }}"> {{ $item->email_sales }} </strong> </a>
                            </address>
                        </div>
                        @endforeach
                        <div class="social-links">
                            <ul class="d-flex">
                                <li>
                                    <a class="rounded-circle" href="https://www.facebook.com/GarudaCyber/?locale=id_ID" title="Facebook">
                                        <i class="fa fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="rounded-circle" href="https://www.instagram.com/garudacyber/?hl=id" title="Twitter">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="rounded-circle" href="https://www.linkedin.com/posts/garuda-cyber-indonesia_hiring-lokerpekanbaru-activity-6998100779475636224-_Y78" title="Linkedin">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="rounded-circle" href="https://www.youtube.com/channel/UC_eKAW0lK_e1hfMg0ooqQPg" title="Youtube">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright-area">
        <div class="container custom-area">
            <div class="row">
                <div class="col-12 text-center col-custom">
                    <div class="copyright-content">
                        @foreach ($footer as $item)
                        <p>{{ $item->footer }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />