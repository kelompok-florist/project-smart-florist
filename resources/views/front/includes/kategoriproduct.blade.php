<!-- Product Countdown Area Start Here -->
<div class="product-countdown-area product-countdown-style pb-text-4">
    <div class="container custom-area">
        <div class="row">
            <!--Section Title Start-->
            <div class="col-12 col-custom">
                <div class="section-title text-center">
                    <h3 class="section-title-3">Daftar Kategori</h3>
                </div>
            </div>
            <!--Section Title End-->
        </div>
        <div class="row product-row">
            <div class="col-12 col-custom">
                <div class="item-carousel-2 swiper-container anime-element-multi product-area">
                    <div class="swiper-wrapper">
                        @forelse ($kategori as $item)
                        <div class="single-item swiper-slide">
                            <!--Single Product Start-->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="{{ route('produk.by.kategori', ['kategoriId' => $item->id]) }}">
                                        <img src="{{ asset ('uploads/' . $item->gambar_kategori) }}" alt="" class="product-image-1 w-100" style="height:350px">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2"> <a href="{{ route('produk.by.kategori', ['kategoriId' => $item->id]) }}">{{ $item->nama_kategori }}</a></h4>
                                        <h4>{{ $item->Product->count() }} items</h4>
                                    </div>
                                </div>
                            </div>
                            <!--Single Product End-->
                        </div>                    
                        @empty
                        <p>no data </p>
                        @endforelse
                    </div>
                    <!-- Slider pagination -->
                    <div class="swiper-pagination default-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product Countdown Area End Here --