<div class="product-area mt-text-2">
    <div class="container custom-area-2 overflow-hidden">
        <div class="row">
            <!--Section Title Start1-->
            <div class="col-12 col-custom mb-30">
                <div class="section-title text-center mb-30">
                    <span class="section-title-1">Wonderful gift</span>
                    <h3 class="section-title-3">Best Seller</h3>
                </div>
                <div class="widget-list widget-mb-1 m-3" style="float: right">
                    <form action="{{ route('allproduct') }}">
                        <button type="submit">See All Product</button>
                    </form>
                </div>
            </div>
            <!--Section Title End-->
        </div>
        <div class="row product-row">
            <div class="col-12 col-custom">
                <div class="product-slider swiper-container anime-element-multi swiper-container-initialized swiper-container-horizontal" style="overflow: hidden">
                    <div class="swiper-wrapper" id="swiper-wrapper-d955398104ceb64dd" aria-live="polite">
                        @foreach($bestSeller->take(6) as $no => $data)
                        <div class="single-item swiper-slide" style="width: 315.333px; margin-right: 10px">
                            <!-- Single Product Start -->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="{{ ('/detail/' . $data->id)}}">
                                        @if ($data->gambars &&
                                        count($data->gambars) > 0)
                                        <img src="{{ asset( $data->gambars[0]->gambar) }}" alt="" class="product-image" width="350" height="220" />
                                        @else
                                        <h5>No Image Uploaded</h5>
                                        @endif
                                    </a>
                                    {{-- <span class="onsale">Sale</span> --}}
                                    <div class="add-action d-flex flex-column position-absolute"></div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="{{ ('/detail/' . $data->id)}}">{{ $data->nama }}</a>
                                            <h5>
                                                <a href="">({{ $data->province->name
                                                    }} / {{ $data->regency->name
                                                    }} )</a>
                                            </h5>
                                        </h4>
                                        {{--
                                        <span>{!! $data->deskripsi !!}</span>
                                        --}}
                                    </div>
                                    <div class="price-box">
                                        <span class="regular-price">Rp.{{ number_format($data->harga,
                                            0, ',', '.') }}</span>
                                    </div>
                                    <a href="{{ ('/detail/' . $data->id)}}" class="btn product-cart">Whatsapp</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- Slider pagination -->
                    <div class="swiper-pagination default-pagination swiper-pagination-clickable swiper-pagination-bullets">
                        <!-- ... (pagination bullets) ... -->
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="row product-row">
            <div class="col-12 col-custom">
                <div class="product-slider swiper-container anime-element-multi swiper-container-initialized swiper-container-horizontal" style="overflow: hidden">
                    <div class="swiper-wrapper" id="swiper-wrapper-d955398104ceb64dd" aria-live="polite">
                        @foreach($bestSeller->skip(5)->take(5) as $no => $data)
                        <div class="single-item swiper-slide" style="width: 315.333px; margin-right: 10px">
                            <!-- Single Product Start -->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="{{ ('/detail/' . $data->id)}}">
                                        @if ($data->gambars &&
                                        count($data->gambars) > 0)
                                        <img src="{{ asset( $data->gambars[0]->gambar) }}" alt="" class="product-image" width="350" height="220" />
                                        @else
                                        <h5>No Image Uploaded</h5>
                                        @endif
                                    </a>
                                    {{-- <span class="onsale">Sale</span> --}}
                                    <div class="add-action d-flex flex-column position-absolute"></div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="{{ ('/detail/' . $data->id)}}">{{ $data->nama }}</a>
                                            <h5>
                                                <a href="">({{ $data->province->name
                                                    }} / {{ $data->regency->name
                                                    }} )</a>
                                            </h5>
                                        </h4>
                                        {{--
                                        <span>{!! $data->deskripsi !!}</span>
                                        --}}
                                    </div>
                                    <div class="price-box">
                                        <span class="regular-price">Rp.{{ number_format($data->harga,
                                            0, ',', '.') }}</span>
                                    </div>
                                    <a href="{{ ('/detail/' . $data->id)}}" class="btn product-cart">Whatsapp</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- Slider pagination -->
                    <div class="swiper-pagination default-pagination swiper-pagination-clickable swiper-pagination-bullets">
                        <!-- ... (pagination bullets) ... -->
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
    </div>
</div>