<!-- History Area Start Here -->
<div class="our-history-area pt-5 mt-text-3">
    <div class="our-history-area">
        <div class="container custom-area">
            <div class="row">
                <!--Section Title Start-->
                <div class="col-12">
                    @foreach ($aboutus as $item)    
                    <div class="section-title text-center mb-30">
                        <span class="section-title-1">{{ $item->title }}</span>
                        <h2 class="section-title-large">{{ $item->subtitle }}</h2>
                    </div>
                    <!--Section Title End-->
                </div>
                {{-- <div class="row">
                    <div class="col-lg-8 ms-auto me-auto">
                        <div class="history-area-content pb-0 mb-0 border-0 text-center">
                            <p>
                                {!! $item->deskripsi !!}
                            </p>
                        </div>
                    </div>
                </div> --}}
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- Feature History Area End Here -->
<!-- About Area Start Here -->
<div class="about-area mt-no-text">
    <div class="container custom-area">
        @foreach ($konten as $item)
        @if ($item->id == 1)
        <div class="row mb-30 align-items-center">
            <div class="col-md-6 col-custom">
                <div class="collection-content">
                    <div class="section-title text-left">
                        <span class="section-title-1">{{ $item->title }}</span>
                        <h3 class="section-title-3 pb-0">{{ $item->subtitle }}</h3>
                    </div>
                    <div class="desc-content">
                        <p>
                            {!! $item->deskripsi !!}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style">
                    <div class="banner-img">
                        <a href="#">
                            <img src="{{ asset('uploads/'. $item->gambar) }}" alt="About Image" style="height: 420px;width:550px">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            @endif
            @endforeach
        </div>
        <br>
        <br>
        @foreach ($konten as $item)
        @if ($item->id==2)
        <div class="row align-items-center">
            <div class="col-md-6 col-custom order-2 order-md-1">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style">
                    <div class="banner-img">
                        <a href="#">
                            <img src="{{ asset('uploads/'. $item->gambar) }}" alt="About Image" style="height: 420px;width:550px">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            <div class="col-md-6 col-custom order-1 order-md-2">
                <div class="collection-content">
                    <div class="section-title text-left">
                        <span class="section-title-1">{{ $item->title }}</span>
                        <h3 class="section-title-3 pb-0">{{ $item->subtitle }}</h3>
                    </div>
                    <div class="desc-content">
                        <p>
                            {!! $item->deskripsi !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>
<br>
<br>
<!-- About Us Area End Here -->
