<style>
    .main-header {
        height: 80px;
    }
</style>
<div class="main-header header-transparent header-sticky sticky">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-2 col-xl-2 col-md-4 col-2 col-custom">
                <div class="header-logo d-flex align-items-center mb-3">
                    @foreach ($aboutus as $item)
                    <a href="/">
                        <img class="img-full" src="{{ asset('uploads/'. $item->logo) }}" alt="Logo Image" style="max-width: 60%; height: auto; max-height: 18mm;">
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-9 d-none d-lg-flex justify-content-center col-custom col-6">
                <nav class="main-nav d-none d-lg-flex">
                    <ul class="nav">
                        <li>
                            <a class="" href="/">
                                <span class="menu-text">Home</span>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <span class="menu-text">Sumatera</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="mega-menu dropdown-hover">
                                <div class="menu-colum">
                                    <ul>
                                        @foreach($provinsi as $data)
                                        @if($data->id_pulau == 1)
                                        <div class="dropdown" style="width: 500%;">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{ $data->name }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div class="row">
                                                    @foreach($kota as $value)
                                                    @if($value->province_id == $data->id)
                                                    <div class="col-md-4">
                                                        <a class="dropdown-item" href="{{ route('front.produk.index', ['id' => ucwords(str_replace(' ', '-', urldecode($value->name)))]) }}">{{ $value->name }}</a>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>

                                        @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="#">
                                <span class="menu-text">Jawa</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="mega-menu dropdown-hover">
                                <div class="menu-colum">
                                    <ul>
                                        @foreach($provinsi as $data)
                                        @if($data->id_pulau == 2)
                                        <div class="dropdown" style="width: 500%;">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{ $data->name }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div class="row">
                                                    @foreach($kota as $value)
                                                    @if($value->province_id == $data->id)
                                                    <div class="col-md-4">
                                                        <a class="dropdown-item" href="{{ route('front.produk.index', ['id' => $value->name]) }}">{{ $value->name }}</a>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="#">
                                <span class="menu-text">Bali</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="mega-menu dropdown-hover">
                                <div class="menu-colum">
                                    <ul>
                                        @foreach($provinsi as $data)
                                        @if($data->id_pulau == 3)
                                        <div class="dropdown" style="width: 500%;">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{ $data->name }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div class="row">
                                                    @foreach($kota as $value)
                                                    @if($value->province_id == $data->id)
                                                    <div class="col-md-4">
                                                        <a class="dropdown-item" href="{{ route('front.produk.index', ['id' => $value->name]) }}">{{ $value->name }}</a>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div> @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="#">
                                <span class="menu-text">Kalimantan</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="mega-menu dropdown-hover">
                                <div class="menu-colum">
                                    <ul>
                                        @foreach($provinsi as $data)
                                        @if($data->id_pulau == 4)
                                        <div class="dropdown" style="width: 500%;">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{ $data->name }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div class="row">
                                                    @foreach($kota as $value)
                                                    @if($value->province_id == $data->id)
                                                    <div class="col-md-4">
                                                        <a class="dropdown-item" href="{{ route('front.produk.index', ['id' => $value->name]) }}">{{ $value->name }}</a>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div> @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="#">
                                <span class="menu-text">Sulawesi</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="mega-menu dropdown-hover">
                                <div class="menu-colum">
                                    <ul>
                                        @foreach($provinsi as $data)
                                        @if($data->id_pulau == 5)
                                        <div class="dropdown" style="width: 500%;">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{ $data->name }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div class="row">
                                                    @foreach($kota as $value)
                                                    @if($value->province_id == $data->id)
                                                    <div class="col-md-4">
                                                        <a class="dropdown-item" href="{{ route('front.produk.index', ['id' => $value->name]) }}">{{ $value->name }}</a>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div> @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="#">
                                <span class="menu-text">Papua</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="mega-menu dropdown-hover">
                                <div class="menu-colum">
                                    <ul>
                                        @foreach($provinsi as $data)
                                        @if($data->id_pulau == 6)
                                        <div class="dropdown" style="width: 500%;">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{ $data->name }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div class="row">
                                                    @foreach($kota as $value)
                                                    @if($value->province_id == $data->id)
                                                    <div class="col-md-4">
                                                        <a class="dropdown-item" href="{{ route('front.produk.index', ['id' => $value->name]) }}">{{ $value->name }}</a>
                                                        <div class="dropdown-divider"></div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div> @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a class="" href="{{ route('contact-us')}}">
                                <span class="menu-text">Contact</span>
                            </a>
                        </li>

                        <li>
                            <a class="" href="{{ route('allproduct') }}">
                                <span class="menu-text">Shop Here</span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
            <div class="col-lg-1 col-md-6 col-4 col-custom">
                <div class="header-right-area main-nav">
                    <ul class="nav">
                        <li class="sidemenu-wrap">
                        </li>
                        <li class="account-menu-wrap d-none d-lg-flex">
                            <a href="#" class="off-canvas-menu-btn">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                        <li class="mobile-menu-btn d-lg-none">
                            <a class="off-canvas-btn" href="#">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>