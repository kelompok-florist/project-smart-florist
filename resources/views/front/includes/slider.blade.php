<style>
  .intro11-content {
    background-color: rgba(255, 255, 255, 0.2); 
    padding: 20px;
    border-radius: 8px;
    color: white; 
  }
</style>
<div class="intro11-slider-wrap section">
  <div class="intro11-slider swiper-container swiper-container-fade swiper-container-initialized swiper-container-horizontal">
    <div class="swiper-wrapper" id="swiper-wrapper-4dffe3edb3f85147" aria-live="polite" style="transition: all 0ms ease 0s;">
      @foreach($slider as $data)
      <!-- slide 6 -->
      <div class="intro11-section swiper-slide slide-bg-1 bg-position swiper-slide-active" data-swiper-slide-index="0" role="group" aria-label="2 / 4" style="width: 1059px; transition: all 0ms ease 0s; opacity: 1; transform: translate3d(-1059px, 0px, 0px); background-image:url('/uploads/{{ $data->gambar_slide }}');">
        <!-- Intro Content Start -->
        <div class=" intro11-content text-left">
          <h1 class="title" style="color:#000000;">
            {{ $data->judul_slide }}
          </h1>
          <h2 class="desc-content" style="color: rgb(0, 0, 0);">
            {!! $data->deskripsi !!}
          </h2>
          @php
          $detailUrl = url('/detail/' . $data->id);
          @endphp
          <a href="{{ 'https://wa.me/' .$data->nowa . '?text=' . urlencode("Hi, saya tertarik dengan produk Anda. $detailUrl") }}" class="btn flosun-button secondary-btn theme-color rounded-0">Whatsapp</a>
        </div>
        <!-- Intro Content End -->
      </div>
      @endforeach
      <!-- slide 3 -->

    </div>
    <!-- Slider Navigation -->
    <div class="home1-slider-prev swiper-button-prev main-slider-nav" tabindex="0" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-4dffe3edb3f85147">
      <i class="lnr lnr-arrow-left"></i>
    </div>
    <div class="home1-slider-next swiper-button-next main-slider-nav" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-4dffe3edb3f85147">
      <i class="lnr lnr-arrow-right"></i>
    </div>
    <!-- Slider pagination -->
    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span></div>
    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
  </div>
</div>