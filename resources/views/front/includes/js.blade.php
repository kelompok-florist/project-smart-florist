<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- jQuery JS -->
<script src="http://127.0.0.1:8000/template/js/vendor/jquery-3.6.0.min.js"></script>
<!-- jQuery Migrate JS -->
<script src="http://127.0.0.1:8000/template/js/vendor/jquery-migrate-3.3.2.min.js"></script>
<!-- Modernizer JS -->
<script src="http://127.0.0.1:8000/template/js/vendor/modernizr-3.7.1.min.js"></script>
<!-- Bootstrap JS -->
<script src="http://127.0.0.1:8000/template/js/vendor/bootstrap.bundle.min.js"></script>

<!-- Swiper Slider JS -->
<script src="http://127.0.0.1:8000/template/js/plugins/swiper-bundle.min.js"></script>
<!-- nice select JS -->
<script src="http://127.0.0.1:8000/template/js/plugins/nice-select.min.js"></script>
<!-- Ajaxchimpt js -->
<script src="http://127.0.0.1:8000/template/js/plugins/jquery.ajaxchimp.min.js"></script>
<!-- Jquery Ui js -->
<script src="http://127.0.0.1:8000/template/js/plugins/jquery-ui.min.js"></script>
<!-- Jquery Countdown js -->
<script src="http://127.0.0.1:8000/template/js/plugins/jquery.countdown.min.js"></script>
<!-- jquery magnific popup js -->
<script src="http://127.0.0.1:8000/template/js/plugins/jquery.magnific-popup.min.js"></script>

<!-- Main JS -->
<script src="http://127.0.0.1:8000/template/js/main.js"></script>