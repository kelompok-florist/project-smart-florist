<!DOCTYPE html>
<html class="no-js" lang="en">
<!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Smart Florist</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/images/logo/logow.png" />

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />
    <!-- Linear Icons CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/linearicons.min.css" />
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/swiper-bundle.min.css" />
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/animate.min.css" />
    <!-- Jquery ui CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/jquery-ui.min.css" />
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/nice-select.min.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/magnific-popup.css" />
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />


    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/style.css" />
</head>



<body style="overflow: visible;" class="">


    <!-- Header Area Start Here -->
    <header class="main-header-area">
        <!-- Main Header Area Start -->
        @include('front.includes.header')
        <!-- Main Header Area End -->

        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product...">
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Sumatera</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                        <li><a href="/Aceh">Aceh</a></li>
                                        <li><a href="/Bangka">Bangka</a></li>
                                        <li><a href="/Batam">Batam</a></li>
                                        <li><a href="/Batu Raja">Batu Raja</a></li>
                                        <li><a href="/Bengkulu">Bengkulu</a></li>
                                        <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Jawa</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 1</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Bandung">Bandung</a></li>
                                                <li><a href="/Banyumas">Banyumas</a></li>
                                                <li><a href="/Cianjur">Cianjur</a></li>
                                                <li><a href="/Cirebon">Cirebon</a></li>
                                                <li><a href="/Garut">Garut</a></li>
                                                <li><a href="/Kebumen">Kebumen</a></li>
                                                <li><a href="/Kediri">Kediri</a></li>
                                                <li><a href="/Kudus">Kudus</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 2</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Magelang">Magelang</a></li>
                                                <li><a href="/Malang">Malang</a></li>
                                                <li><a href="/Pekalongan">Pekalongan</a></li>
                                                <li><a href="/Purbalingga">Purbalingga</a></li>
                                                <li><a href="/Purwokerto">Purwokerto</a></li>
                                                <li><a href="/Rempang">Rempang</a></li>
                                                <li><a href="/Semarang">Semarang</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 3</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Solo">Solo</a></li>
                                                <li><a href="/Sukabumi">Sukabumi</a></li>
                                                <li><a href="/Surabaya">Surabaya</a></li>
                                                <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                <li><a href="/Tegal">Tegal</a></li>
                                                <li><a href="/Wonogiri">Wonogiri</a></li>
                                                <li><a href="/Wonosobo">Wonosobo</a></li>
                                                <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Kalimantan</a>
                                    <ul class="dropdown">
                                        <li><a href="/Pontianak">Pontianak</a></li>
                                        <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                        <li><a href="/Samarinda">Samarinda</a></li>
                                        <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                        <li><a href="/Balipapan">Balikpapan</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Sulawesi</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Makasar">Makasar</a></li>
                                        <li><a href="/Palu">Palu</a></li>
                                        <li><a href="Kendari">Kendari</a></li>
                                        <li><a href="/Manado">Manado</a></li>
                                        <li><a href="/Gorontalo">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Bali&Lombok</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Bali">Bali</a></li>
                                        <li><a href="/Denpasar">Denpasar</a></li>
                                        <li><a href="/Lombok">Lombok</a></li>
                                        <li><a href="/Mataram">Mataram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">

                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="#">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="#">smartflorist@gmail.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->

        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="off-canvas-inner">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <ul class="menu-top-menu">
                            <li><a href="about-us.html">About Us</a></li>
                        </ul>
                        <p class="desc-content">
                            Smart Florist, sebuah platform florist online yang menghadirkan keindahan bunga dengan sentuhan teknologi modern. Di balik antarmuka yang ramah pengguna, Smart Florist tidak hanya menawarkan koleksi bunga yang elegan dan beragam, tetapi juga memberikan pengalaman belanja yang cerdas. Dengan fitur pencarian yang pintar dan pengiriman yang efisien, pelanggan dapat dengan mudah menemukan dan mendapatkan rangkaian bunga yang sesuai dengan setiap momen istimewa. Inovatif dalam pendekatan teknologinya, Smart Florist membawa seni floristika ke era digital, menjadikan setiap pembelian bunga sebuah pengalaman yang indah dan terkoneksi secara modern.</strong>
                        </p>
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Indonesia</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">$ USD</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Rp</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="#">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="#">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>

        <!-- Slider -->
        @include('front.includes.slider')
        <!-- Slider End -->

        {{-- TEMPAT EDIT PRODUK --}}

        <div class="product-area mt-text-2">
            <div class="container custom-area-2 overflow-hidden">
                <div class="row product-row">
                    <div class="col-12 col-custom">
                        <div class="product-area mt-text-3">
                            <div class="container custom-area-2 overflow-hidden">
                                <div class="row">
                                    <!--Section Title Start-->
                                    <div class="col-12 col-custom">
                                        <div class="section-title text-center mb-30">
                                            <span class="section-title-1">Karangan Bunga Best Seller</span>
                                            <h3 class="section-title-3">{{ $kotaSearch->name }}</h3>
                                        </div>
                                    </div>
                                    <!--Section Title End-->
                                </div>
                                <div class="row product-row">
                                    <div class="col-12 col-custom">
                                        <div class="product-slider swiper-container anime-element-multi">
                                            <div class="swiper-wrapper">
                                                @foreach($produk as $data)
                                                <div class="swiper-slide">
                                                    <div class="single-product position-relative mb-30">
                                                        <div class="product-image">
                                                            <a class="d-block" href="{{ ('/detail/' . $data->id)}}">
                                                                @if ($data->gambars && count($data->gambars) > 0)
                                                                <img src="{{ asset( $data->gambars[0]->gambar) }}" alt="" class="product-image-1 w-100" height="220">
                                                                @else
                                                                <h5>No Image Uploaded</h5>
                                                                @endif
                                                            </a>
                                                            <div class="add-action d-flex flex-column position-absolute">
                                                            </div>
                                                        </div>
                                                        <div class="product-content">
                                                            <div class="product-title">
                                                                <h4 class="title-2">
                                                                    <a href="{{ ('detail/' . $data->id)}}">{{$data->nama}}</a>
                                                                    <h5><a href="">({{ $data->province->name }} / {{ $data->regency->name }} )</a>
                                                                    </h5>
                                                                </h4>
                                                                <span>{!! $data->deskripsi !!}</span>
                                                            </div>
                                                            <div class="price-box">
                                                                <span class="regular-price">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                                            </div>
                                                            <a href="#" class="btn product-cart">WHATSAPP</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>

                                            <!-- Slider pagination -->
                                            <div class="swiper-pagination default-pagination"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- PRODUK END HERE --}}

        <div class="container">
            <div class="row">
                <div>
                    <p><strong>TOKO BUNGA {{ $kotaSearch->name }}</strong></p>
                    <p>Toko Bunga di {{ $kotaSearch->name }}. Florist {{ $kotaSearch->name }}. Hotline 081807077070. Toko Karangan Bunga {{ $kotaSearch->name }}.</p>

                    <p><strong>Area pelayanan:</strong></p>
                    <ul>
                        <li>{{ $kotaSearch->name }} Selatan; Kebayoran Baru, Kebayoran Lama, Pesanggrahan, Cilandak, Pasar Minggu Jagakarsa, Mampang Prapatan, Pancoran, Tebet, Setiabudi.</li>
                        <li>{{ $kotaSearch->name }} Barat; Taman Sari, Tambora, Palmerah, Grogol Petamburan, Kebon Jeruk, Kembangan, Cengkareng, Kalideres.</li>
                        <li>{{ $kotaSearch->name }} Timur; Matraman, Pulo Gadung, Jatinegara, Duren Sawit, Kramat Jati, Makasar, Pasar Rebo, Ciracas, Cipayung, Cakung.</li>
                        <li>{{ $kotaSearch->name }} Pusat; Gambir, Tanah Abang, Menteng, Senen, Cempaka Putih, Kemayoran, Sawah Besar, Johar Baru.</li>
                        <li>{{ $kotaSearch->name }} Utara; Cilincing, Koja, Kelapa Gading, Tanjung Priok, Pademangan, Penjaringan.</li>
                        <li>Cibubur, Taman Mini Indonesia Indah, Bintaro.</li>
                    </ul>

                    <p><strong>Bunga Papan {{ $kotaSearch->name }} memiliki beragam jenis Karangan Bunga seperti:</strong></p>
                    <ul>
                        <li>Karangan bunga duka cita</li>
                        <li>Bunga papan duka cita</li>
                        <li>Bunga ucapan selamat</li>
                        <li>Bunga papan pernikahan</li>
                        <li>Bunga papan anniversary</li>
                        <li>Bunga papan congratulations</li>
                    </ul>

                    <p>Kami jual bunga papan dengan jaminan desain yang bagus dan pemakaian bunga segar...</p>


                    <address>
                        <strong>smartflorist.com</strong><br>
                        Toko Bunga {{ $kotaSearch->name }}<br>
                        Jl. Petojo No. 30, Cideng, {{ $kotaSearch->name }} Pusat<br>
                        Hotline. 081268074036 (Whatsapp)<br>
                        Email. smartflorist@gmail.com
                    </address>
                </div>
            </div>
        </div>

        <!--Footer Area Start-->
        @include('front.includes.footer')
        <!--Footer Area End-->

        <!-- Modal -->
        <div class="modal flosun-modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                        <span class="close-icon" aria-hidden="true">x</span>
                    </button>
                    <div class="modal-body">
                        <div class="container-fluid custom-area">
                            <div class="row">
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product-img">
                                        <a class="w-100" href="#">
                                            <img class="w-100" src="http://127.0.0.1:8000/template/images/product/large-size/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top show" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>
        <!-- Scroll to Top End -->

        <!-- JS -->
        @include('front.includes.js')
        <!-- End JS -->
        @yield('content')
</body>

</html>