    <!DOCTYPE html>
    <html class="no-js" lang="en">
    <!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>Smart Florist</title>
        <meta name="robots" content="noindex, follow" />
        <meta name="description" content="" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/images/logo/logow.png" />
        <link rel="stylesheet" href="{{asset('template')}}/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/vendor/linearicons.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/plugins/swiper-bundle.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/plugins/animate.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/plugins/jquery-ui.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/plugins/nice-select.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/plugins/magnific-popup.css" />
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />
        <link rel="stylesheet" href="{{asset('template')}}/css/style.css" />
    </head>

    <body style="overflow: visible;" class="">
        <header class="main-header-area">
            
            {{-- Header --}}
            @include('front.includes.header')
            {{-- Header End --}}
            
            <aside class="off-canvas-wrapper" id="mobileMenu">
                <div class="off-canvas-overlay"></div>
                <div class="off-canvas-inner-content">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="off-canvas-inner">
                        <div class="search-box-offcanvas">
                            <form>
                                <input type="text" placeholder="Search product...">
                                <button class="search-btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <div class="mobile-navigation">
                            <nav>
                                <ul class="mobile-menu">
                                    <li class="menu-item-has-children">
                                        <a href="#">Sumatera</a>
                                        <ul class="megamenu dropdown" style="display: none;">
                                            <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <li><a href="/Aceh">Aceh</a></li>
                                            <li><a href="/Bangka">Bangka</a></li>
                                            <li><a href="/Batam">Batam</a></li>
                                            <li><a href="/Batu Raja">Batu Raja</a></li>
                                            <li><a href="/Bengkulu">Bengkulu</a></li>
                                            <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                        <a href="#">Jawa</a>
                                        <ul class="megamenu dropdown" style="display: none;">
                                            <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                                <a href="#">Slide 1</a>
                                                <ul class="dropdown" style="display: none;">
                                                    <li><a href="/Bandung">Bandung</a></li>
                                                    <li><a href="/Banyumas">Banyumas</a></li>
                                                    <li><a href="/Cianjur">Cianjur</a></li>
                                                    <li><a href="/Cirebon">Cirebon</a></li>
                                                    <li><a href="/Garut">Garut</a></li>
                                                    <li><a href="/Kebumen">Kebumen</a></li>
                                                    <li><a href="/Kediri">Kediri</a></li>
                                                    <li><a href="/Kudus">Kudus</a></li>
                                                </ul>
                                            </li>
                                            <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                                <a href="#">Slide 2</a>
                                                <ul class="dropdown" style="display: none;">
                                                    <li><a href="/Magelang">Magelang</a></li>
                                                    <li><a href="/Malang">Malang</a></li>
                                                    <li><a href="/Pekalongan">Pekalongan</a></li>
                                                    <li><a href="/Purbalingga">Purbalingga</a></li>
                                                    <li><a href="/Purwokerto">Purwokerto</a></li>
                                                    <li><a href="/Rempang">Rempang</a></li>
                                                    <li><a href="/Semarang">Semarang</a></li>
                                                </ul>
                                            </li>
                                            <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                                <a href="#">Slide 3</a>
                                                <ul class="dropdown" style="display: none;">
                                                    <li><a href="/Solo">Solo</a></li>
                                                    <li><a href="/Sukabumi">Sukabumi</a></li>
                                                    <li><a href="/Surabaya">Surabaya</a></li>
                                                    <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                    <li><a href="/Tegal">Tegal</a></li>
                                                    <li><a href="/Wonogiri">Wonogiri</a></li>
                                                    <li><a href="/Wonosobo">Wonosobo</a></li>
                                                    <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                        <a href="#">Kalimantan</a>
                                        <ul class="dropdown">
                                            <li><a href="/Pontianak">Pontianak</a></li>
                                            <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                            <li><a href="/Samarinda">Samarinda</a></li>
                                            <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                            <li><a href="/Balipapan">Balikpapan</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                        <a href="#">Sulawesi</a>
                                        <ul class="dropdown" style="display: none;">
                                            <li><a href="/Makasar">Makasar</a></li>
                                            <li><a href="/Palu">Palu</a></li>
                                            <li><a href="Kendari">Kendari</a></li>
                                            <li><a href="/Manado">Manado</a></li>
                                            <li><a href="/Gorontalo">Gorontalo</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                        <a href="#">Bali&Lombok</a>
                                        <ul class="dropdown" style="display: none;">
                                            <li><a href="/Bali">Bali</a></li>
                                            <li><a href="/Denpasar">Denpasar</a></li>
                                            <li><a href="/Lombok">Lombok</a></li>
                                            <li><a href="/Mataram">Mataram</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="offcanvas-widget-area">
                            <div class="top-info-wrap text-left text-black">
                                <ul class="address-info">
                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <a href="#">(1245) 2456 012</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <a href="#">smartflorist@gmail.com</a>
                                    </li>
                                </ul>
                                <div class="widget-social">
                                    <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                    <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                    <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                    <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            
            <aside class="off-canvas-menu-wrapper" id="sideMenu">
                <div class="off-canvas-overlay"></div>
                <div class="off-canvas-inner-content">
                    <div class="off-canvas-inner">
                        <div class="btn-close-off-canvas">
                            <i class="fa fa-times"></i>
                        </div>
                        <div class="offcanvas-widget-area">
                            <ul class="menu-top-menu">
                                <li><a href="about-us.html">About Us</a></li>
                            </ul>
                            <p class="desc-content">
                                Smart Florist, sebuah platform florist online yang menghadirkan keindahan bunga dengan sentuhan teknologi modern. Di balik antarmuka yang ramah pengguna, Smart Florist tidak hanya menawarkan koleksi bunga yang elegan dan beragam, tetapi juga memberikan pengalaman belanja yang cerdas. Dengan fitur pencarian yang pintar dan pengiriman yang efisien, pelanggan dapat dengan mudah menemukan dan mendapatkan rangkaian bunga yang sesuai dengan setiap momen istimewa. Inovatif dalam pendekatan teknologinya, Smart Florist membawa seni floristika ke era digital, menjadikan setiap pembelian bunga sebuah pengalaman yang indah dan terkoneksi secara modern.</strong>
                            </p>
                            <div class="switcher">
                                <div class="language">
                                    <span class="switcher-title">Language: </span>
                                    <div class="switcher-menu">
                                        <ul>
                                            <li>
                                                <a href="#">English</a>
                                                <ul class="switcher-dropdown">
                                                    <li><a href="#">Indonesia</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="currency">
                                    <span class="switcher-title">Currency: </span>
                                    <div class="switcher-menu">
                                        <ul>
                                            <li>
                                                <a href="#">$ USD</a>
                                                <ul class="switcher-dropdown">
                                                    <li><a href="#">Rp</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="top-info-wrap text-left text-black">
                                <ul class="address-info">
                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <a href="#">(1245) 2456 012</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <a href="#">info@yourdomain.com</a>
                                    </li>
                                </ul>
                                <div class="widget-social">
                                    <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                    <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                    <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                    <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- offcanvas widget area end -->
                    </div>
                </div>
            </aside>
            
            @include('front.includes.slider')
            
            <div class="shop-main-area">
                <div class="container container-default custom-area">
                    <div class="row flex-row-reverse">
                        <div class="col-lg-9 col-12 col-custom widget-mt">
                            <!--shop toolbar start-->
                            <div class="shop_toolbar_wrapper mb-30">
                                <div class="shop_toolbar_btn">
                                    <button data-role="grid_3" type="button" class="active btn-grid-3" title="Grid"><i class="fa fa-th"></i></button>
                                    <button data-role="grid_list" type="button" class="btn-list" title="List"><i class="fa fa-th-list"></i></button>
                                </div>
                            </div>
                            <div id="searchResults" class="row shop_wrapper grid_3">
                                @forelse($produk as $data)
                                
                                <div class="col-md-6 col-sm-6 col-lg-4 col-custom product-area produk-card" data-id="{{ $data->regencies_id }}" data-title="{{ $data->nama }}">
                                    <div class="product-item">
                                        <div class="single-product position-relative mr-0 ml-0">
                                            <div class="product-image">
                                                <a class="d-block" href="{{ ('/detail/' . $data->id)}}">
                                                    @if ($data->gambars && count($data->gambars) > 0)
                                                    <img src="{{ asset( $data->gambars[0]->gambar) }}" alt="" class="product-image-1 w-100" height="220">
                                                    @else
                                                    <h5>No Image Uploaded</h5>
                                                    @endif
                                                </a>
                                                <span class="onsale">Sale!</span>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2"> <a href="{{ ('/detail/' . $data->id)}}">{{$data->nama}}</a></h4>
                                                    <h5><a href="">({{ $data->province->name }} / {{ $data->regency->name }} )</a>
                                                    </h5>
                                                </div>
                                                <div class="price-box">
                                                    <span class="regular-price ">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                                </div>
                                                <a href="{{ ('/detail/' . $data->id)}}" class="btn product-cart">Order Now</a>
                                            </div>
                                            <div class="product-content-listview">
                                                <div class="product-title">
                                                    <h4 class="title-2"> <a href="{{ ('/detail/' . $data->id)}}">{{$data->nama}}</a></h4>
                                                    <h5><a href="">({{ $data->province->name }} / {{ $data->regency->name }} )</a>
                                                    </h5>
                                                </div>
                                                <div class="price-box">
                                                    <span class="regular-price ">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                                </div>
                                                <p class="desc-content">{!! $data->deskripsi !!}</p>
                                                <div class="button-listview">
                                                    @php
                                                    $detailUrl = url('/detail/' . $data->id);
                                                    @endphp
                                                    <a href="{{ 'https://wa.me/' .$data->nowa . '?text=' . urlencode("Hi, saya tertarik dengan produk Anda. $detailUrl") }}" class="btn product-cart button-icon flosun-button dark-btn" data-toggle="tooltip" data-placement="top" title="Add to Cart"> <span><i class="fa fa-whatsapp"></i> Order Now </span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <p class="text-center fs-4">No Produk Found</p>
                                @endforelse
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-custom">
                                    <div class="toolbar-bottom custom-pagination">
                                        {{ $produk->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- INCLUDE SEARCH -->
                        @include('front.produk.search')
                    </div>
                </div>
            </div>
            <br>
            <br>

            @include('front.includes.footer')
            <a class="scroll-to-top show" href="#">
                <i class="lnr lnr-arrow-up"></i>
            </a>
            @include('front.includes.js')
            @yield('content')
            <script>
                $(function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $('#provinces').on('change', function() {
                        let id_provinsi = $('#provinces').val();

                        $.ajax({
                            type: 'POST',
                            url: "{{ route('getKabupaten') }}",
                            data: {
                                id_provinsi: id_provinsi
                            },
                            cache: false,

                            success: function(msg) {
                                $('#kabupaten').html(msg);
                            },
                            error: function(data) {
                                console.log('error:', data)
                            },
                        })
                    });
                });
            </script>
    </body>

    </html>