<div class="col-lg-3 col-12 col-custom">
    <aside class="sidebar_widget widget-mt">
        <div class="widget_inner">
            <form action="{{ route('allproduct') }}">
            <div class="widget-list widget-mb-1">
                <h3 class="widget-title">Search</h3>
                <div class="search-box">
                    <div class="input-group">
                        <input type="text" id="searchInput" class="form-control produk-card" placeholder="Search " name="search" value="{{ request('search') }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            </form> 
            <form action="" method="get">
                @csrf
            <div class="widget-list widget-mb-1">
                <h3 class="widget-title" style="margin-bottom: 20px;">Wilayah</h3>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="provinces">Provinsi </label>
                    <select id="provinces" name="provinces_id" class="form-control">
                        <option value="" selected>Cari Provinsi...</option>
                        @foreach ($provinsi as $row)
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="kabupaten">Kabupaten </label>
                    <select name="regencies_id" id="kabupaten" class="form-control">
                        <option>Pilih Kabupaten..</option>
                    </select>
                </div>
                <div class="widget-list widget-mb-1 m-3">
                        <button type="submit">Submit</button>
                    </div>
                </form>
            </div>
            

            <div class="widget-list widget-mb-1">
                <h3 class="widget-title">Categories</h3>
                <div class="sidebar-body">
                    <ul class="sidebar-list">
                        <li><a href="/allproduct">All Produk</a></li>
                        @forelse ($kategori as $item)
                        <li><a href="{{ route('produk.by.kategori', ['kategoriId' => $item->id]) }}">{{ $item->nama_kategori }} ({{ $item->Product->count() }})</a></li>
                        @empty
                        <p>no data </p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </form>
        </div>
    </aside>
</div>