<!DOCTYPE html>
<html class="no-js" lang="en">
<!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Smart Florist</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/images/logo/logow.png" />

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />
    <!-- Linear Icons CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/linearicons.min.css" />
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/swiper-bundle.min.css" />
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/animate.min.css" />
    <!-- Jquery ui CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/jquery-ui.min.css" />
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/nice-select.min.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/magnific-popup.css" />
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />


    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/style.css" />
</head>



<body style="overflow: visible;" class="">


    <!-- Header Area Start Here -->
    <header class="main-header-area">
        <!-- Main Header Area Start -->
        @include('front.includes.header')
        <!-- Main Header Area End -->

        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product...">
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Sumatera</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                        <li><a href="/Aceh">Aceh</a></li>
                                        <li><a href="/Bangka">Bangka</a></li>
                                        <li><a href="/Batam">Batam</a></li>
                                        <li><a href="/Batu Raja">Batu Raja</a></li>
                                        <li><a href="/Bengkulu">Bengkulu</a></li>
                                        <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Jawa</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 1</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Bandung">Bandung</a></li>
                                                <li><a href="/Banyumas">Banyumas</a></li>
                                                <li><a href="/Cianjur">Cianjur</a></li>
                                                <li><a href="/Cirebon">Cirebon</a></li>
                                                <li><a href="/Garut">Garut</a></li>
                                                <li><a href="/Kebumen">Kebumen</a></li>
                                                <li><a href="/Kediri">Kediri</a></li>
                                                <li><a href="/Kudus">Kudus</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 2</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Magelang">Magelang</a></li>
                                                <li><a href="/Malang">Malang</a></li>
                                                <li><a href="/Pekalongan">Pekalongan</a></li>
                                                <li><a href="/Purbalingga">Purbalingga</a></li>
                                                <li><a href="/Purwokerto">Purwokerto</a></li>
                                                <li><a href="/Rempang">Rempang</a></li>
                                                <li><a href="/Semarang">Semarang</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 3</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Solo">Solo</a></li>
                                                <li><a href="/Sukabumi">Sukabumi</a></li>
                                                <li><a href="/Surabaya">Surabaya</a></li>
                                                <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                <li><a href="/Tegal">Tegal</a></li>
                                                <li><a href="/Wonogiri">Wonogiri</a></li>
                                                <li><a href="/Wonosobo">Wonosobo</a></li>
                                                <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Kalimantan</a>
                                    <ul class="dropdown">
                                        <li><a href="/Pontianak">Pontianak</a></li>
                                        <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                        <li><a href="/Samarinda">Samarinda</a></li>
                                        <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                        <li><a href="/Balipapan">Balikpapan</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Sulawesi</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Makasar">Makasar</a></li>
                                        <li><a href="/Palu">Palu</a></li>
                                        <li><a href="Kendari">Kendari</a></li>
                                        <li><a href="/Manado">Manado</a></li>
                                        <li><a href="/Gorontalo">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Bali&Lombok</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Bali">Bali</a></li>
                                        <li><a href="/Denpasar">Denpasar</a></li>
                                        <li><a href="/Lombok">Lombok</a></li>
                                        <li><a href="/Mataram">Mataram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">

                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">smartflorist@gmail.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->

        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="off-canvas-inner">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <ul class="menu-top-menu">
                            <li><a href="about-us.html">About Us</a></li>
                        </ul>
                        <p class="desc-content">
                            Smart Florist, sebuah platform florist online yang menghadirkan keindahan bunga dengan sentuhan teknologi modern. Di balik antarmuka yang ramah pengguna, Smart Florist tidak hanya menawarkan koleksi bunga yang elegan dan beragam, tetapi juga memberikan pengalaman belanja yang cerdas. Dengan fitur pencarian yang pintar dan pengiriman yang efisien, pelanggan dapat dengan mudah menemukan dan mendapatkan rangkaian bunga yang sesuai dengan setiap momen istimewa. Inovatif dalam pendekatan teknologinya, Smart Florist membawa seni floristika ke era digital, menjadikan setiap pembelian bunga sebuah pengalaman yang indah dan terkoneksi secara modern.</strong>
                        </p>
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Indonesia</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">$ USD</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Rp</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>

        <!-- Slider -->
        @include('front.includes.slider')
        <!-- Slider End -->

        <!-- INI DETAIL -->
        <div class="single-product-main-area" style="margin-bottom: 50px">
            <div class="container container-default custom-area">
                <div class="row">
                    <div class="col-lg-5 offset-lg-0 col-md-8 offset-md-2 col-custom">
                        <div class="product-details-img">
                            <div class="single-product-img swiper-container gallery-top popup-gallery">
                                <div class="swiper-wrapper">
                                    @foreach ($gambars as $data)

                                    <div class="swiper-slide">
                                        <a class="w-100" href="{{ asset( $data->gambar) }}" target="_blank" class="product-image d-block" width="250" height="250">
                                            <img class="w-100" style="max-height: 500px;" src="{{ asset( $data->gambar) }}" alt="product-image">
                                        </a>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                            <div class="single-product-thumb swiper-container gallery-thumbs">
                                <div class="swiper-wrapper">

                                    @foreach ($gambars as $data)
                                    <div class="swiper-slide">
                                        <img src="{{ asset($data->gambar) }}" class="product-image" width="100" height="100">
                                    </div>

                                    @endforeach

                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next swiper-button-white"><i class="lnr lnr-arrow-right"></i></div>
                                <div class="swiper-button-prev swiper-button-white"><i class="lnr lnr-arrow-left"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-custom">
                        <div class="product-summery position-relative">
                            <div class="product-head mb-3">
                                <h1 class="product-title">{{ $produk->nama}}</h1>
                            </div>
                            <div class="price-box mb-2">
                                <i>
                                    <p class="product-title"> {{ $produk->province->name}} / {{ $produk->regency->name}} </p>
                                </i>
                                <span class="regular-price">Rp.{{ number_format($produk->harga, 0, ',', '.') }}</span>
                            </div>
                            <span>{!! $produk->deskripsi !!}</span>
                            <div class="quantity-with_btn mb-5">
                                <div class="quantity">
                                </div>
                                <div class="add-to_cart">
                                    @php
                                        $detailUrl = url('/detail/' . $produk->id);
                                    @endphp
                                    <a class="btn product-cart button-icon flosun-button dark-btn" href="{{ 'https://wa.me/' .$produk->nowa . '?text=' . urlencode("Hi, saya tertarik dengan produk Anda. $detailUrl") }}">
                                        <i class="fa fa-whatsapp"></i> WhatsApp
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-no-text">
                    <div class="col-lg-12 col-custom">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active text-uppercase" id="home-tab" data-bs-toggle="tab" href="#connect-1" role="tab" aria-selected="true">Shipping Policy</a>
                            </li>

                        </ul>
                        <div class="tab-content mb-text" id="myTabContent">
                            <div class="tab-pane fade show active" id="connect-1" role="tabpanel" aria-labelledby="home-tab">
                                <div class="desc-content">
                                    <p class="mb-3">Selamat datang di SmartFlorist! Kami senang bisa menjadi pilihan Anda untuk kebutuhan bunga dan pengiriman bunga.
                                        Kami berkomitmen untuk memberikan pengalaman pengiriman yang terbaik kepada pelanggan kami.
                                        Berikut adalah detail kebijakan pengiriman kami:<br>

                                        Kami melayani pengiriman bunga di [kota/kawasan] dan sekitarnya. Harap pastikan alamat pengiriman Anda berada dalam area layanan kami sebelum melanjutkan pesanan.<br>

                                        Biaya pengiriman akan dihitung berdasarkan lokasi pengiriman dan jenis layanan yang Anda pilih. Detail biaya akan diberikan pada saat pemesanan dan dapat langsung diakses melalui tim kami.<br>

                                        Pesanan yang diterima sebelum pukul [waktu] akan diproses dan dikirim pada hari yang sama, kecuali ada instruksi khusus dari Anda.
                                        Waktu pengiriman dapat dipengaruhi oleh kondisi cuaca dan lalu lintas, namun, kami akan berusaha sebaik mungkin untuk memenuhi kebutuhan waktu pengiriman Anda.<br>

                                        Kami menyediakan opsi pengiriman standar dan ekspres. Pilihan metode pengiriman dapat Anda pilih sesuai kebutuhan Anda.<br>

                                        Anda akan menerima nomor pelacakan setelah pesanan Anda dikirim. Dengan nomor ini, Anda dapat melacak status pengiriman pesanan Anda.<br>

                                        Jika pesanan Anda tiba dalam kondisi yang tidak memuaskan atau ada keluhan terkait pengiriman, harap hubungi kami dalam [jumlah hari] setelah menerima pesanan.
                                        Kami akan berusaha untuk menyelesaikan masalah dengan memberikan penggantian atau pengembalian dana sesuai kebijakan kami.<br>

                                        Untuk pertanyaan lebih lanjut atau bantuan terkait pengiriman, jangan ragu untuk menghubungi tim layanan pelanggan kami melalui WhatsApp di 01234 567 890 atau melalui email di smartflorist@gmail.com.
                                        Terima kasih atas dukungan Anda terhadap SmartFlorist! Kami berharap dapat memberikan pengalaman pengiriman bunga yang luar biasa bagi Anda.


                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Detail Product End Here -->

        <!--Footer Area Start-->
        @include('front.includes.footer')
        <!--Footer Area End-->

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top show" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>
        <!-- Scroll to Top End -->

        <!-- JS -->
        @include('front.includes.js')
        <!-- End JS -->
</body>

</html>