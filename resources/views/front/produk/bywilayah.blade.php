<!DOCTYPE html>
<html class="no-js" lang="en">
<!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Smart Florist</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/images/logo/logow.png" />

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />
    <!-- Linear Icons CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/linearicons.min.css" />
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/swiper-bundle.min.css" />
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/animate.min.css" />
    <!-- Jquery ui CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/jquery-ui.min.css" />
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/nice-select.min.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/magnific-popup.css" />
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />


    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/style.css" />
</head>



<body style="overflow: visible;" class="">


    <!-- Header Area Start Here -->
    <header class="main-header-area">
        <!-- Main Header Area Start -->
        @include('front.includes.header')
        <!-- Main Header Area End -->

        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product...">
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Sumatera</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                        <li><a href="/Aceh">Aceh</a></li>
                                        <li><a href="/Bangka">Bangka</a></li>
                                        <li><a href="/Batam">Batam</a></li>
                                        <li><a href="/Batu Raja">Batu Raja</a></li>
                                        <li><a href="/Bengkulu">Bengkulu</a></li>
                                        <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Jawa</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 1</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Bandung">Bandung</a></li>
                                                <li><a href="/Banyumas">Banyumas</a></li>
                                                <li><a href="/Cianjur">Cianjur</a></li>
                                                <li><a href="/Cirebon">Cirebon</a></li>
                                                <li><a href="/Garut">Garut</a></li>
                                                <li><a href="/Kebumen">Kebumen</a></li>
                                                <li><a href="/Kediri">Kediri</a></li>
                                                <li><a href="/Kudus">Kudus</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 2</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Magelang">Magelang</a></li>
                                                <li><a href="/Malang">Malang</a></li>
                                                <li><a href="/Pekalongan">Pekalongan</a></li>
                                                <li><a href="/Purbalingga">Purbalingga</a></li>
                                                <li><a href="/Purwokerto">Purwokerto</a></li>
                                                <li><a href="/Rempang">Rempang</a></li>
                                                <li><a href="/Semarang">Semarang</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 3</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Solo">Solo</a></li>
                                                <li><a href="/Sukabumi">Sukabumi</a></li>
                                                <li><a href="/Surabaya">Surabaya</a></li>
                                                <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                <li><a href="/Tegal">Tegal</a></li>
                                                <li><a href="/Wonogiri">Wonogiri</a></li>
                                                <li><a href="/Wonosobo">Wonosobo</a></li>
                                                <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Kalimantan</a>
                                    <ul class="dropdown">
                                        <li><a href="/Pontianak">Pontianak</a></li>
                                        <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                        <li><a href="/Samarinda">Samarinda</a></li>
                                        <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                        <li><a href="/Balipapan">Balikpapan</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Sulawesi</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Makasar">Makasar</a></li>
                                        <li><a href="/Palu">Palu</a></li>
                                        <li><a href="Kendari">Kendari</a></li>
                                        <li><a href="/Manado">Manado</a></li>
                                        <li><a href="/Gorontalo">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Bali&Lombok</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Bali">Bali</a></li>
                                        <li><a href="/Denpasar">Denpasar</a></li>
                                        <li><a href="/Lombok">Lombok</a></li>
                                        <li><a href="/Mataram">Mataram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">

                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">smartflorist@gmail.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->

        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="off-canvas-inner">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <ul class="menu-top-menu">
                            <li><a href="about-us.html">About Us</a></li>
                        </ul>
                        <p class="desc-content">
                            Smart Florist, sebuah platform florist online yang menghadirkan keindahan bunga dengan sentuhan teknologi modern. Di balik antarmuka yang ramah pengguna, Smart Florist tidak hanya menawarkan koleksi bunga yang elegan dan beragam, tetapi juga memberikan pengalaman belanja yang cerdas. Dengan fitur pencarian yang pintar dan pengiriman yang efisien, pelanggan dapat dengan mudah menemukan dan mendapatkan rangkaian bunga yang sesuai dengan setiap momen istimewa. Inovatif dalam pendekatan teknologinya, Smart Florist membawa seni floristika ke era digital, menjadikan setiap pembelian bunga sebuah pengalaman yang indah dan terkoneksi secara modern.</strong>
                        </p>
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Indonesia</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">$ USD</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Rp</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>

        <!-- Slider -->
        @include('front.includes.slider')
        <!-- Slider End -->

        {{-- TEMPAT EDIT PRODUK --}}
        <div class="shop-main-area">
            <div class="container container-default custom-area">
                <div class="row flex-row-reverse">
                    <div class="col-lg-9 col-12 col-custom widget-mt">
                        <!--shop toolbar start-->
                        <div class="shop_toolbar_wrapper mb-30">
                            <div class="shop_toolbar_btn">
                                <button data-role="grid_3" type="button" class="active btn-grid-3" title="Grid"><i class="fa fa-th"></i></button>
                                <button data-role="grid_list" type="button" class="btn-list" title="List"><i class="fa fa-th-list"></i></button>
                            </div>
                        </div>
                        <!--shop toolbar end-->
                        <!-- Shop Wrapper Start -->
                        <div class="row shop_wrapper grid_3">
                            @foreach($produk as $data)
                            <div class="col-md-6 col-sm-6 col-lg-4 col-custom product-area">
                                <div class="product-item">
                                    <div class="single-product position-relative mr-0 ml-0">
                                        <div class="product-image">
                                            <a class="d-block" href="{{ ('/detail/' . $data->id)}}">
                                                @if ($data->gambars && count($data->gambars) > 0)
                                                <img src="{{ asset( $data->gambars[0]->gambar) }}" alt="" class="product-image-1 w-100" height="220">
                                                @else
                                                <h5>No Image Uploaded</h5>
                                                @endif
                                            </a>
                                            <span class="onsale">Sale!</span>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2"> <a href="{{ ('/detail/' . $data->id)}}">{{$data->nama}}</a></h4>
                                                <h5><a href="">({{ $data->province->name }} / {{ $data->regency->name }} )</a>
                                                </h5>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price ">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                            </div>
                                            <a href="{{ ('/detail/' . $data->id)}}" class="btn product-cart">Order Now</a>
                                        </div>
                                        <div class="product-content-listview">
                                            <div class="product-title">
                                                <h4 class="title-2"> <a href="{{ ('/detail/' . $data->id)}}">{{$data->nama}}</a></h4>
                                                <h5><a href="">({{ $data->province->name }} / {{ $data->regency->name }} )</a>
                                                </h5>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price ">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                            </div>
                                            <p class="desc-content">{!! $data->deskripsi !!}</p>
                                            <div class="button-listview">
                                                @php
                                                $detailUrl = url('/detail/' . $data->id);
                                                @endphp
                                                <a href="{{ 'https://wa.me/' .$data->nowa . '?text=' . urlencode("Hi, saya tertarik dengan produk Anda. $detailUrl") }}" class="btn product-cart button-icon flosun-button dark-btn" data-toggle="tooltip" data-placement="top" title="Add to Cart"> <span><i class="fa fa-whatsapp"></i> Order Now </span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- Shop Wrapper End -->
                        <!-- Bottom Toolbar Start -->
                        <div class="row">
                            <div class="col-sm-12 col-custom">
                                <div class="toolbar-bottom">
                                    <div class="pagination">
                                        <ul>
                                            <li class="current">1</li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li class="next"><a href="#">next</a></li>
                                            <li><a href="#">&gt;&gt;</a></li>
                                        </ul>
                                    </div>
                                    <p class="desc-content text-center text-sm-right mb-0">Showing 1 - 12 of 34 result</p>
                                </div>
                            </div>
                        </div>
                        <!-- Bottom Toolbar End -->
                    </div>
                    <div class="col-lg-3 col-12 col-custom">
                        <!-- Sidebar Widget Start -->
                        <aside class="sidebar_widget widget-mt">
                            <div class="widget_inner">
                                <div class="widget-list widget-mb-1">
                                    <h3 class="widget-title">Search</h3>
                                    <div class="search-box">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search Our Store" aria-label="Search Our Store">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-list widget-mb-1">
                                    <h3 class="widget-title">Wilayah</h3>
                                    <!-- Widget Menu Start -->
                                    <nav>
                                        <ul class="mobile-menu p-0 m-0">
                                            <li class="menu-item-has-children"><a href="#">Birthday Boqutets</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Aster</a></li>
                                                    <li><a href="#">Aubrieta</a></li>
                                                    <li><a href="#">Basket of Gold</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Funeral Flowers</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Cleome</a></li>
                                                    <li><a href="#">Columbine</a></li>
                                                    <li><a href="#">Coneflower</a></li>
                                                    <li><a href="#">Corepsis</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Interior Decor</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Calendula</a></li>
                                                    <li><a href="#">Castor Bean</a></li>
                                                    <li><a href="#">Catmint</a></li>
                                                    <li><a href="#">Chives</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Custom Orders</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Lily</a></li>
                                                    <li><a href="#">Rose</a></li>
                                                    <li><a href="#">Sunflower</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Custom Orders</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Lily</a></li>
                                                    <li><a href="#">Rose</a></li>
                                                    <li><a href="#">Sunflower</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"><a href="#">Custom Orders</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Lily</a></li>
                                                    <li><a href="#">Rose</a></li>
                                                    <li><a href="#">Sunflower</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                    <!-- Widget Menu End -->
                                </div>
                                {{-- <div class="widget-list widget-mb-1">
                                    <h3 class="widget-title">Price Filter</h3>
                                    <!-- Widget Menu Start -->
                                    <form action="#">
                                        <div id="slider-range"></div>
                                        <button type="submit">Filter</button>
                                        <input type="text" name="text" id="amount" />
                                    </form>
                                    <!-- Widget Menu End -->
                                </div> --}}
                                <div class="widget-list widget-mb-1">
                                    <h3 class="widget-title">Categories</h3>
                                    <div class="sidebar-body">
                                        <ul class="sidebar-list">
                                            <li><a href="{{ route('allproduct') }}">All Product</a></li>
                                            @forelse ($kategori as $item)
                                            <li><a href="{{ route('produk.by.kategori', ['kategoriId' => $item->id]) }}">{{ $item->nama_kategori }} ({{ $item->Product->count() }})</a></li>
                                            @empty
                                            <p>no data </p>
                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                                {{-- <div class="widget-list mb-0">
                                    <h3 class="widget-title">Recent Products</h3>
                                    <div class="sidebar-body">
                                        <div class="sidebar-product align-items-center">
                                            <a href="product-details.html" class="image">
                                                <img src="assets/images/cart/1.jpg" alt="product">
                                            </a>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2"> <a href="product-details.html">Glory of the Snow</a></h4>
                                                </div>
                                                <div class="price-box">
                                                    <span class="regular-price ">$80.00</span>
                                                    <span class="old-price"><del>$90.00</del></span>
                                                </div>
                                                <div class="product-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sidebar-product align-items-center">
                                            <a href="product-details.html" class="image">
                                                <img src="assets/images/cart/2.jpg" alt="product">
                                            </a>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2"> <a href="product-details.html">Pearly Everlasting</a></h4>
                                                </div>
                                                <div class="price-box">
                                                    <span class="regular-price ">$50.00</span>
                                                    <span class="old-price"><del>$60.00</del></span>
                                                </div>
                                                <div class="product-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sidebar-product align-items-center">
                                            <a href="product-details.html" class="image">
                                                <img src="assets/images/cart/3.jpg" alt="product">
                                            </a>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2"> <a href="product-details.html">Jack in the Pulpit</a></h4>
                                                </div>
                                                <div class="price-box">
                                                    <span class="regular-price ">$40.00</span>
                                                    <span class="old-price"><del>$50.00</del></span>
                                                </div>
                                                <div class="product-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </aside>
                        <!-- Sidebar Widget End -->
                    </div>
                </div>
            </div>
        </div>
        {{-- PRODUK END HERE --}}

        <!--Footer Area Start-->
        @include('front.includes.footer')
        <!--Footer Area End-->

        <!-- Modal -->
        <div class="modal flosun-modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                        <span class="close-icon" aria-hidden="true">x</span>
                    </button>
                    <div class="modal-body">
                        <div class="container-fluid custom-area">
                            <div class="row">
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product-img">
                                        <a class="w-100" href="#">
                                            <img class="w-100" src="http://127.0.0.1:8000/template/images/product/large-size/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top show" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>
        <!-- Scroll to Top End -->

        <!-- JS -->
        @include('front.includes.js')
        <!-- End JS -->
        @yield('content')
</body>

</html>