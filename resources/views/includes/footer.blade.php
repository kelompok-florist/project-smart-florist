<footer class="footer" data-background-color="dark2">
        <div class="copyright ml-auto" style="color: white">
            2023, made with <i class="fa fa-heart heart text-danger"></i> by <a style="color: white" href="/dashboard">Smart Florist</a>
        </div>				
    </div>
</footer>