<div class="sidebar sidebar-style-2" data-background-color="dark">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ asset('back/img/profile.png') }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            Smart-Florist
                            <span class="user-level">{{ Auth::user()->name }}</span>
                        </span>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item active">
                    <a href="/dashboard">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Manajement Data</h4>
                </li>
                <li class="nav-item">
                    <a href="{{ route('product.index') }}">
                        <i class="fas fa-cube"></i>
                        <p>Data Produk</p>
                        <span class="badge badge-success"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}">
                        <i class="fas fa-cubes"></i>
                        <p>Data Kategori</p>
                        <span class="badge badge-success"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('slide.index') }}">
                        <i class="far fa-images"></i>
                        <p>Data Slider</p>
                        <span class="badge badge-success"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('konten.index') }}">
                        <i class="fab fa-adversal"></i>
                        <p>Data Konten</p>
                        <span class="badge badge-success"></span>
                    </a>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#sidebarLayouts">
                        <i class="fas fa-th-list"></i>
                        <p>Data Lainnya</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="sidebarLayouts">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="{{ route('aboutus.index') }}">
                                    <span class="sub-item">Data About Us</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('contact.index') }}">
                                    <span class="sub-item">Data Contact</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('sosmed.index') }}">
                                    <span class="sub-item">Data Sosmed</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('footer.index') }}">
                                    <span class="sub-item">Data Footer</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fas fa-undo"></i>
                        <p>{{ __('Logout') }}</p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>