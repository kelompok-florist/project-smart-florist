<?php

use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SlideController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FooterController;
use App\Http\Controllers\KontenController;
use App\Http\Controllers\SosmedController;
use App\Models\Kategori;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [HomeController::class, 'index']);


Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('product', ProductController::class);
    Route::resource('kategori', KategoriController::class);
    Route::resource('slide', SlideController::class);
    Route::resource('konten', KontenController::class);
    Route::resource('aboutus', AboutUsController::class);
    Route::resource('contact', ContactController::class);
    Route::resource('sosmed', SosmedController::class);
    Route::resource('footer', FooterController::class);
});
Route::post('/getKabupaten', [ProductController::class, 'getKabupaten'])->name('getKabupaten');

//route contactus
Route::get('/contact-us', [HomeController::class, 'showContactForm'])->name('contact-us');

//route allproduct
Route::get('/allproduct', [HomeController::class, 'showAllProduct'])->name('allproduct');


//route produk
Route::get('/{id}', [HomeController::class, 'produk'])->name('front.produk.index');
Route::get('/detail/{id}', [HomeController::class, 'detail']);

//route produk sesuai wilayah




//route produk sesuai kategori
Route::get('/produk/kategori/{kategoriId}', [HomeController::class, 'produkByKategori'])->name('produk.by.kategori');
