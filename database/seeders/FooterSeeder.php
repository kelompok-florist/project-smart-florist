<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Data Dummy Untuk Footer
        DB::table('footer')->insert([
            'footer' => 'Kelompok Smart-Florist © 2023 | Garuda Cyber Institute',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
