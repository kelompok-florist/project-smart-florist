<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SosmedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('sosmed')->insert([
            'nama_website' => 'smartfloris.com',
            'whatsapp' => '0123 456 789',
            'email_sales' => 'contoh@example.com',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
