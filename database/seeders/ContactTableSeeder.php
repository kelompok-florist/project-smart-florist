<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('contact')->insert([
            'alamat' => 'Alamat Contoh',
            'phone' => '123456789',
            'email' => 'contoh@example.com',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
