<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use AzisHapidin\IndoRegion\RawDataGetter;
use Illuminate\Support\Facades\DB;

class IndoRegionProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @deprecated
     *
     * @return void
     */
    public function run()
    {
        // Get Data
        $provinces = RawDataGetter::getProvinces();

        // Add default value for id_pulau if it doesn't exist
        foreach ($provinces as &$province) {
            switch ($province['name']) {
                case 'ACEH':
                case 'SUMATERA UTARA':
                case 'SUMATERA BARAT':
                case 'RIAU':
                case 'JAMBI':
                case 'SUMATERA SELATAN':
                case 'BENGKULU':
                case 'LAMPUNG':
                case 'KEPULAUAN BANGKA BELITUNG':
                case 'KEPULAUAN RIAU':
                    $province['id_pulau'] = 1;
                    break;

                case 'BANTEN':
                case 'DKI JAKARTA':
                case 'JAWA BARAT':
                case 'JAWA TENGAH':
                case 'DI YOGYAKARTA':
                case 'JAWA TIMUR':
                    $province['id_pulau'] = 2;
                    break;

                case 'BALI':
                case 'NUSA TENGGARA BARAT':
                case 'NUSA TENGGARA TIMUR':
                    $province['id_pulau'] = 3;
                    break;

                case 'KALIMANTAN BARAT':
                case 'KALIMANTAN TENGAH':
                case 'KALIMANTAN SELATAN':
                case 'KALIMANTAN TIMUR':
                case 'KALIMANTAN UTARA':
                    $province['id_pulau'] = 4;
                    break;

                case 'SULAWESI UTARA':
                case 'SULAWESI TENGAH':
                case 'SULAWESI SELATAN':
                case 'SULAWESI TENGGARA':
                case 'GORONTALO':
                case 'SULAWESI BARAT':
                    $province['id_pulau'] = 5;
                    break;

                case 'PAPUA':
                case 'PAPUA BARAT':
                case 'MALUKU UTARA':
                case 'MALUKU':
                    $province['id_pulau'] = 6;
                    break;

                default:
                    // Atur nilai default untuk provinsi lain jika diperlukan
                    $province['id_pulau'] = null;
                    break;
            }
        }

        // Insert Data to Database
        DB::table('provinces')->insert($provinces);
    }
}
