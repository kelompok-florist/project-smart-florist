<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KontenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate dummy data for 'konten' table
        $kontenData = [
            [
                'title' => 'Judul Konten 1',
                'subtitle' => 'Subtitle Konten 1',
                'deskripsi' => 'Deskripsi konten 1.',
                'gambar' => 'gambar1.jpg',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'Judul Konten 2',
                'subtitle' => 'Subtitle Konten 2',
                'deskripsi' => 'Deskripsi konten 2.',
                'gambar' => 'gambar2.jpg',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // Add more data as needed
        ];

        // Insert data into 'konten' table
        DB::table('konten')->insert($kontenData);
    }
}
