<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->call(KategoriSeeder::class);
        $this->call(KontenSeeder::class);
        $this->call(SosmedTableSeeder::class);
        $this->call(AboutUsSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(FooterSeeder::class);
        $this->call(SlideSeeder::class);
        $this->call(IndoRegionSeeder::class);
    }
}
