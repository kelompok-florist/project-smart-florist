<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Data slide
        $slideData = [
            [
                'judul_slide' => 'Slide 1',
                'deskripsi' => 'Deskripsi slide 1',
                'gambar_slide' => 'public/template/images/slider/1-1.jpg',
                'nowa' => '082287605169',
            ],
            [
                'judul_slide' => 'Slide 2',
                'deskripsi' => 'Deskripsi slide 2',
                'gambar_slide' => 'public/template/images/slider/1-2.jpg',
                'nowa' => '082287605169',

            ],
            [
                'judul_slide' => 'Slide 3',
                'deskripsi' => 'Deskripsi slide 3',
                'gambar_slide' => 'public/template/images/slider/1-3.jpg',
                'nowa' => '082287605169',

            ],
        ];

        // Insert data ke tabel slide
        DB::table('slide')->insert($slideData);
    }
}
