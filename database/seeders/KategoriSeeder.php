<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Data kategori
        $kategoriData = [
            [
                'nama_kategori' => 'Papan Bunga',
                'slug' => 'papan-bunga',
                'gambar_kategori' => 'papan-bunga.jpg',
            ],
            [
                'nama_kategori' => 'Bunga Meja',
                'slug' => 'bunga-meja',
                'gambar_kategori' => 'bunga-meja.jpg',
            ],
            [
                'nama_kategori' => 'Flower Box',
                'slug' => 'flower-box',
                'gambar_kategori' => 'flowerbox.jpg',
            ],
            [
                'nama_kategori' => 'Standing Flower',
                'slug' => 'standing-flower',
                'gambar_kategori' => 'standingflower.jpg',
            ],
        ];

        // Insert data ke tabel kategori
        DB::table('kategori')->insert($kategoriData);
    }
}
