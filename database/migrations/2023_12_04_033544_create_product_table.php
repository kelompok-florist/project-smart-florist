<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('slug');
            $table->integer('kategori_id');
            $table->integer('provinces_id');
            $table->integer('regencies_id');
            $table->integer('harga');
            $table->text('deskripsi');
            $table->text('nowa');
            $table->tinyInteger('bestseller')->default('0')->comment('1=bestseller,0=regular');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product');
    }
};
