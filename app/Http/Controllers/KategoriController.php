<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;


class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('back.kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_kategori' => 'required',
            'gambar_kategori' => 'mimes:png,jpg,webp,jpeg,gif,bmp',
        ]);

        $newname = 'kategori-' . time() . '.jpg';
        $data = request()->all();
        // $data['gambar_kategori'] = $request->file('gambar_kategori')->store('kategori');
        $data['slug'] = Str::slug($request->nama_kategori);

        if ($request->hasFile('gambar_kategori')) {
            $data['gambar_kategori'] = $request->file('gambar_kategori')->storeAs('kategori', $newname);
        }

        Kategori::create($data);

        Alert::success('Sukses', 'Data Berhasil Ditambah');
        return redirect()->route('kategori.index');
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $kategori = Kategori::find($id);

        return view('back.kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'nama_kategori' => 'required',
            'gambar_kategori' => 'mimes:png,jpg,jpeg,gif,bmp'
        ]);

        if (empty($request->file('gambar_kategori'))) {
            $kategori = Kategori::find($id);
            $kategori->update([
                'nama_kategori' => $request->nama_kategori,
                'slug' => Str::slug($request->nama_kategori),
            ]);
            return redirect()->route('kategori.index')->with('success', 'Data Berhasil DiUpdaate');
        } else {
            $kategori = Kategori::find($id);
            Storage::delete($kategori->gambar_kategori);
            $kategori->update([
                'nama_kategori' => $request->nama_kategori,
                'slug' => Str::slug($request->nama_kategori),
                'gambar_kategori' => $request->file('gambar_kategori')->store('kategori'),
            ]);

            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('kategori.index');
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $kategori = Kategori::find($id);

        Storage::delete($kategori->gambar_kategori);

        $kategori->delete();

        Alert::success('Success', 'Data Berhasil Dihapus');
        return redirect()->route('kategori.index');
    }
}
