<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Gambar;
use App\Models\Product;
use App\Models\Province;
use App\Models\Regency;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::all();

        return view('back.product.index', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $kategori = Kategori::all();
        $provinces = Province::all();
        // $regencies = Regency::all();

        return view('back.product.create', [
            'kategori' => $kategori,
            'provinces' => $provinces,
            // 'regencies' => $regencies,
        ]);
    }

    public function getKabupaten(Request $request)
    {
        $id_provinsi = $request->id_provinsi;

        $kabupatens = Regency::where('province_id', $id_provinsi)->get();

        foreach ($kabupatens as $kabupaten) {
            echo "<option value ='$kabupaten->id'>$kabupaten->name</option>";
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData =  $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required|numeric',
            'kategori_id' => 'required|integer',
            'nowa' => 'required|numeric',
            'gambar' => 'nullable',
        ], [
            'nama.required' => 'Kolom Nama wajib diisi.',
            'deskripsi.required' => 'Kolom Deskripsi wajib diisi.',
            'harga.required' => 'Kolom Harga wajib diisi.',
            'harga.numeric' => 'Kolom Harga harus berupa angka.',
            'nowa.required' => 'Kolom No WA wajib diisi.',
            'nowa.numeric' => 'Kolom No WA harus berupa angka.',
            'nowa.min' => 'Kolom No WA harus memiliki panjang minimal 8 karakter.',
        ]);

        $kategori = Kategori::findOrFail($validatedData['kategori_id']);

        $product = $kategori->product()->create([
            'kategori_id' => $validatedData['kategori_id'],
            'nama' => $validatedData['nama'],
            'slug' => Str::slug($request->input('nama')),
            'deskripsi' => $validatedData['deskripsi'],
            'harga' => $validatedData['harga'],
            'bestseller' => $request->bestseller == true ? '1' : '0',
            'nowa' => '62' . $request->input('nowa'),
            'provinces_id' => $request->input('provinces_id'),
            'regencies_id' => $request->input('regencies_id'),
        ]);

        if ($request->hasFile('gambar')) {
            $uploadPath = 'uploads/products/';

            $i = 1;
            foreach ($request->file('gambar') as $imageFile) {
                $extension = $imageFile->getClientOriginalExtension();
                $filename = time() . $i++ . '.' . $extension;
                $imageFile->move($uploadPath, $filename);
                $finalImagePathName = $uploadPath . '/' . $filename;

                $product->gambars()->create([
                    'product_id' => $product->id,
                    'gambar' => $finalImagePathName,
                ]);
            }
        }

        Alert::success('Success', 'Data Berhasil Ditambah');
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::find($id);
        $kategori = Kategori::all();
        $regencies = Regency::all();
        $provinces = Province::all();

        return view('back.product.edit', [
            'product' => $product,
            'kategori' => $kategori,
            'regencies' => $regencies,
            'provinces' => $provinces,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $product_id)
    {
        $validatedData =  $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required|numeric',
            'kategori_id' => 'required|integer',
            'nowa' => 'required|numeric',
            'gambar' => 'nullable',
        ], [
            'nama.required' => 'Kolom Nama wajib diisi.',
            'deskripsi.required' => 'Kolom Deskripsi wajib diisi.',
            'harga.required' => 'Kolom Harga wajib diisi.',
            'harga.numeric' => 'Kolom Harga harus berupa angka.',
            'nowa.required' => 'Kolom No WA wajib diisi.',
            'nowa.numeric' => 'Kolom No WA harus berupa angka.',
            'nowa.min' => 'Kolom No WA harus memiliki panjang minimal 8 karakter.',
        ]);

        $product = Product::where('id', $product_id)->first();

        if ($product) {
            $product->update([
                'kategori_id' => $validatedData['kategori_id'],
                'nama' => $validatedData['nama'],
                'slug' => Str::slug($request->input('nama')),
                'deskripsi' => $validatedData['deskripsi'],
                'harga' => $validatedData['harga'],
                'bestseller' => $request->bestseller == true ? '1' : '0',
                'nowa' => '62' . $request->input('nowa'),
                'provinces_id' => $request->input('provinces_id'),
                'regencies_id' => $request->input('regencies_id'),
            ]);

            if ($request->hasFile('gambar')) {
                $uploadPath = 'uploads/products/';

                $i = 1;
                foreach ($request->file('gambar') as $imageFile) {
                    $extension = $imageFile->getClientOriginalExtension();
                    $filename = time() . $i++ . '.' . $extension;
                    $imageFile->move($uploadPath, $filename);
                    $finalImagePathName = $uploadPath . '/' . $filename;

                    $product->gambars()->create([
                        'product_id' => $product->id,
                        'gambar' => $finalImagePathName,
                    ]);
                }
            }

            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('product.index');
        } else {
            Alert::success('Success', 'Product tidak ditemukan');
            return redirect()->route('product.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $product_id)
    {
        $product = Product::findOrFail($product_id);
        if ($product->gambars) {
            foreach ($product->gambars as $image) {
                if (File::exists($image->gambar)) {
                    File::delete($image->gambar);
                }
            }

            $product->delete();
            Alert::success('Success', 'Product Berhasil Dihapus');
            return redirect()->route('product.index');
        }
    }

    // $product = Product::find($id);

    // // Check if there are existing images before attempting to delete them
    // if ($product->gambars->isNotEmpty()) {
    //     foreach ($product->gambars as $gambar) {
    //         Storage::delete($gambar->gambar);
    //         $gambar->delete();
    //     }
    // }

    // $product->delete();

    // Alert::success('Success', 'Data Berhasil Dihapus');
    // return redirect()->route('product.index');

    public function destroyImage(int $product_image_id)
    {
        $gambar = Gambar::findOrFail($product_image_id);
        if (File::exists($gambar->gambar)) {
            File::delete($gambar->gambar);
        }
        $gambar->delete();
        Alert::success('Success', 'Gambar Product Berhasil Dihapus');
        return redirect()->back();
    }
}
