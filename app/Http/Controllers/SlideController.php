<?php

namespace App\Http\Controllers;

use App\Models\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $slide = Slide::all();

        return view('back.slide.index', compact('slide'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul_slide' => 'required|string|max:255',
            'gambar_slide' => 'nullable|image|mimes:png,jpg,webp,jpeg,gif,bmp',
            'nowa' => ['required', 'numeric', 'regex:/^8/'],
            'deskripsi' => 'required|string',
        ], [
            'judul_slide.required' => 'Judul slide harus diisi.',
            'judul_slide.max' => 'Judul slide tidak boleh lebih dari :max karakter.',
            'gambar_slide.image' => 'File harus berupa gambar.',
            'gambar_slide.mimes' => 'Format gambar tidak valid. Pilih format yang sesuai: png, jpg, webp, jpeg, gif, bmp.',
            'gambar_slide.max' => 'Ukuran gambar tidak boleh lebih dari :max kilobyte.',
            'nowa.required' => 'Nomor WhatsApp harus diisi.',
            'nowa.numeric' => 'Nomor WhatsApp harus berupa angka.',
            'nowa.regex' => 'Nomor WhatsApp harus dimulai dengan angka 8.',
            'deskripsi.required' => 'Deskripsi harus diisi.',
            'deskripsi.string' => 'Deskripsi harus berupa teks.',
        ]);

        $newname = 'slide-' . time() . '.jpg';
        $data = $request->all();
        if ($request->hasFile('gambar_slide')) {
            $data['gambar_slide'] = $request->file('gambar_slide')->storeAs('slide', $newname);
        }

        // Menambahkan prefix "62" ke nomor WhatsApp
        $data['nowa'] = '62' . $data['nowa'];

        Slide::create($data);

        Alert::success('Success', 'Data Berhasil Ditambah');
        return redirect()->route('slide.index');
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $slide = Slide::find($id);


        return view('back.slide.edit', [
            'slide' => $slide,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'judul_slide' => 'required|string|max:255',
            'gambar_slide' => 'nullable|image|mimes:png,jpg,webp,jpeg,gif,bmp',
            'nowa' => 'required|numeric',
            'deskripsi' => 'required|string',
        ], [
            'judul_slide.required' => 'Judul slide harus diisi.',
            'judul_slide.max' => 'Judul slide tidak boleh lebih dari :max karakter.',
            'gambar_slide.image' => 'File harus berupa gambar.',
            'gambar_slide.mimes' => 'Format gambar tidak valid. Pilih format yang sesuai: png, jpg, webp, jpeg, gif, bmp.',
            'gambar_slide.max' => 'Ukuran gambar tidak boleh lebih dari :max kilobyte.',
            'nowa.required' => 'Nomor WhatsApp harus diisi.',
            'nowa.numeric' => 'Nomor WhatsApp harus berupa angka.',
            'deskripsi.required' => 'Deskripsi harus diisi.',
            'deskripsi.string' => 'Deskripsi harus berupa teks.',
        ]);

        if (empty($request->file('gambar_slide'))) {
            $slide = Slide::find($id);
            $slide->update([
                'judul_slide' => $request->judul_slide,
                'deskripsi' => $request->deskripsi,
                'nowa' => $request->nowa,
            ]);
            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('slide.index');
        } else {
            $slide = Slide::find($id);
            Storage::delete($slide->gambar_slide);
            $slide->update([
                'judul_slide' => $request->judul_slide,
                'deskripsi' => $request->deskripsi,
                'nowa' => $request->nowa,
                'gambar_slide' => $request->file('gambar_slide')->store('slide'),
            ]);

            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('slide.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $slide = Slide::find($id);

        Storage::delete($slide->gambar_slide);

        $slide->delete();

        Alert::success('Success', 'Data Berhasil Dihapus');
        return redirect()->route('slide.index');
    }
}
