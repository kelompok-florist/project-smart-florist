<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\Contact;
use App\Models\Footer;
use App\Models\Kategori;
use App\Models\Konten;
use App\Models\Gambar;
use App\Models\Product;
use App\Models\Regency;
use App\Models\Province;
use App\Models\Slide;
use App\Models\Sosmed;
use Illuminate\Http\Request;

use function Laravel\Prompts\search;

class HomeController extends Controller
{
    protected $slider;

    public function __construct(Slide $slider)
    {
        $this->slider = $slider;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bestSeller = Product::where('bestseller', '1')->get();
        $produk = Product::all();
        $provinsi = Province::all();
        $kota = Regency::all();
        $kategori = Kategori::all();
        $aboutus = AboutUs::all();
        $konten = Konten::all();
        $contact = Contact::all();
        $sosmed = Sosmed::all();
        $footer = Footer::all();

        return view('front.home')
            ->with('produk', $produk)
            ->with('provinsi', $provinsi)
            ->with('kota', $kota)
            ->with('kategori', $kategori)
            ->with('aboutus', $aboutus)
            ->with('konten', $konten)
            ->with('contact', $contact)
            ->with('sosmed', $sosmed)
            ->with('footer', $footer)
            ->with('bestSeller', $bestSeller)
            ->with('slider', $this->slider->all());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function produk($id)
    {
        $kotaSearch = Regency::where('name', ucwords(str_replace('-', ' ', $id)))->first();

        if ($kotaSearch) {
            $produk = Product::where('regencies_id', $kotaSearch->id)
                ->where('bestseller', '1') // Kondisi tambahan
                ->latest()
                ->get();
        } else {
            // Jika $kotaSearch tidak ada, hanya ambil produk bestseller
            $produk = Product::where('bestseller', '1')
                ->latest()
                ->take(12)
                ->get();
        }


        $kota = Regency::all();
        $provinsi = Province::all();
        $kategori = Kategori::all();
        $aboutus = AboutUs::all();
        $contact = Contact::all();
        $sosmed = Sosmed::all();
        $footer = Footer::all();

        return view('front.produk.index', compact('produk', 'provinsi', 'kota', 'kategori', 'kotaSearch', 'aboutus', 'contact', 'sosmed', 'footer'))
            ->with('slider', $this->slider->all());
    }

    // Other methods remain unchanged

    public function detail($id)
    {
        $produk = Product::with('gambars')->findOrFail($id);
        $provinsi = Province::all();
        $kota = Regency::all();
        $kategori = Kategori::all();
        $aboutus = AboutUs::all();
        $contact = Contact::all();
        $sosmed = Sosmed::all();
        $footer = Footer::all();

        return view('front.produk.detail-product', [
            'produk' => $produk,
            'provinsi' => $provinsi,
            'kota' => $kota,
            'kategori' => $kategori,
            'gambars' => $produk->gambars,
            'aboutus' => $aboutus,
            'contact' => $contact,
            'sosmed' => $sosmed,
            'footer' => $footer,
            'slider' => $this->slider->all(),
        ]);
    }


    public function produkByKategori($id)
    {
        // Cari kategori berdasarkan ID
        $kategori = Kategori::with('Product')->find($id);

        $produk = $kategori->Product();

        if (request('search')) {
            $search = request('search');
            $produk->where('nama', 'like', '%' . $search . '%')
                ->orWhere('deskripsi', 'like', '%' . $search . '%')
                ->orWhereHas('regency', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                })
                ->orWhereHas('province', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                });
        }

        $produk = $produk->paginate(9);

        $provinsi = Province::all();
        $kota = Regency::all();
        $kategori = Kategori::all();
        $aboutus = AboutUs::all();
        $konten = Konten::all();
        $contact = Contact::all();
        $sosmed = Sosmed::all();
        $footer = Footer::all();

        // Render view dan kirim data ke view
        return view('front.produk.bykategori', compact('produk', 'provinsi', 'kota', 'kategori', 'konten', 'aboutus', 'contact', 'sosmed', 'footer'))
            ->with('slider', $this->slider->all());
    }


    public function showContactForm()
    {
        $produk = Product::all();
        $provinsi = Province::all();
        $kota = Regency::all();
        $kategori = Kategori::all();
        $aboutus = AboutUs::all();
        $contact = Contact::all();
        $sosmed = Sosmed::all();
        $footer = Footer::all();

        return view('front.includes.contact-us', compact('provinsi', 'kota', 'produk', 'aboutus', 'contact', 'sosmed', 'footer'));
    }

    public function showAllProduct(Request $request)
    {
        $regencies_id = $request->get('regencies_id');
        $provinsi = Province::all();
        $kota = Regency::all();
        $kategori = Kategori::all();
        $aboutus = AboutUs::all();
        $konten = Konten::all();
        $contact = Contact::all();
        $sosmed = Sosmed::all();
        $footer = Footer::all();
        $slider = Slide::all();
        $products = Product::latest()
            ->filter(request(['search', 'kategori', 'regencies_id']))
            ->paginate(9)
            ->withQueryString();



        return view('front.produk.allproduct', [
            'produk' => $products,
            'provinsi' => $provinsi,
            'kota' => $kota,
            'kategori' => $kategori,
            'aboutus' => $aboutus,
            'konten' => $konten,
            'contact' => $contact,
            'sosmed' => $sosmed,
            'footer' => $footer,
            'slider' => $slider,
            'regencies_id' => $regencies_id,
        ]);
    }

    public function filter($filters)
    {
        // $filters adalah array yang berisi nilai dari 'search', 'kategori', dan 'regencies_id'
        return $this->where(function ($query) use ($filters) {
            if ($filters['search']) {
                $query->where('nama_kolom', 'like', '%' . $filters['search'] . '%');
            }

            if ($filters['kategori']) {
                $query->where('kategori', $filters['kategori']);
            }

            // Tambahan logika untuk regencies_id
            if ($filters['regencies_id']) {
                $query->where('regencies_id', $filters['regencies_id']);
            }
        });
    }
}
