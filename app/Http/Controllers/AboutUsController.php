<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $aboutus = AboutUs::all();
        return view('back.aboutUs.index', compact('aboutus'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'required',
            'deskripsi' => 'required',
            'logo' => 'mimes:png,jpg,jpeg,gif,bmp'
        ]);

        $newname = 'aboutus-' . time() . '.jpg';
        $data = request()->all();

        if ($request->hasFile('logo')) {
            $data['logo'] = $request->file('logo')->storeAs('aboutus', $newname);
        }

        AboutUs::create($data);

        Alert::success('Sukses', 'Data Berhasil Ditambah');
        return redirect()->route('aboutus.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(AboutUs $aboutUs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $aboutus = AboutUs::find($id);

        return view('back.aboutus.edit', compact('aboutus'));
    }

    /**
     * Update the specified resource in storage.
     */
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'deskripsi' => 'required',
            'logo' =>  'mimes:png,jpg,jpeg,gif,bmp',
        ]);

        if (empty($request->file('logo'))) {
            $aboutus = AboutUs::find($id);
            $aboutus->update([
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'deskripsi' => $request->deskripsi,
            ]);
            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('aboutus.index');
        } else {
            $aboutus = AboutUs::find($id);
            Storage::delete($aboutus->logo);
            $aboutus->update([
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'deskripsi' => $request->deskripsi,
                'logo' => $request->file('logo')->store('aboutus'),
            ]);

            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('aboutus.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
