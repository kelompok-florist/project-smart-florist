<?php

namespace App\Http\Controllers;

use App\Models\Konten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class KontenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $konten = Konten::all();
        return view('back.konten.index', compact('konten'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $konten = Konten::find($id);

        return view('back.konten.edit', compact('konten'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'required',
            'gambar' => 'mimes:png,jpg,jpeg,gif,bmp'
        ]);

        if (empty($request->file('gambar'))) {
            $Konten = Konten::find($id);
            $Konten->update([
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'deskripsi' => $request->deskripsi,

            ]);
            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('konten.index');
        } else {
            $konten = Konten::find($id);
            Storage::delete($konten->gambar);
            $konten->update([
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'deskripsi' => $request->deskripsi,
                'gambar' => $request->file('gambar')->store('konten'),
            ]);

            Alert::success('Success', 'Data Berhasil Diupdate');
            return redirect()->route('konten.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        ///
    }
}
