<?php

namespace App\Http\Controllers;

use App\Models\Sosmed;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class SosmedController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sosmed = Sosmed::all();
        return view('back.sosmed.index', compact('sosmed'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // return view('back.sosmed.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'nama_website' => 'required',
        //     'whatsapp' => 'required',
        //     'email_sales' => 'required',
        // ]);

        // $sosmed = Sosmed::create([
        //     'nama_website' => $request->nama_website,
        //     'whatsapp' => $request->whatsapp,
        //     'email_sales' => $request->email_sales,
        // ]);

        // Alert::success('Success', 'Data Berhasil Disimpan');
        // return redirect()->route('sosmed.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $sosmed = Sosmed::find($id);

        return view('back.sosmed.edit', compact('sosmed'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama_website' => 'required',
            'whatsapp' => 'required|numeric|max:9999999999999',
            'email_sales' => 'required|email',
        ]);

        $data = $request->all();

        $sosmed = Sosmed::findOrFail($id);
        $sosmed->update($data);

        Alert::success('Success', 'Data Berhasil Diupdate');
        return redirect()->route('sosmed.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // $sosmed = Sosmed::find($id);

        // $sosmed->delete();

        // Alert::success('Success', 'Data Berhasil Dihapus');
        // return redirect()->route('sosmed.index');
    }
}
