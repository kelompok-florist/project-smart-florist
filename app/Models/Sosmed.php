<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sosmed extends Model
{
    use HasFactory;

    protected $table = 'sosmed';

    protected $fillable = [
        'nama_website', 'whatsapp', 'email_sales'
    ];

    protected $hidden = [];
}
