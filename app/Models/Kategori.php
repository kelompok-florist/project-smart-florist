<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'kategori';

    protected $fillable = [
        'nama_kategori', 'slug', 'gambar_kategori'
    ];

    protected $hidden = [];

    public function Product()
    {
        return $this->hasMany(Product::class, 'kategori_id');
    }
}
