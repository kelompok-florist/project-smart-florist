<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function Laravel\Prompts\search;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';

    protected $fillable = [
        'nama',
        'slug',
        'kategori_id',
        'provinces_id',
        'regencies_id',
        'harga',
        'deskripsi',
        'nowa',
        'bestseller',
    ];

    public function scopeFilter($query, array $filters)
    {

        $query->when($filters['search'] ??  false, function ($query, $search) {
            return $query->where('nama', 'like', '%' . $search . '%')
                ->orWhere('deskripsi', 'like', '%' . $search . '%');
        });

        $query->when($filters['kategori'] ?? false, function ($query, $kategori) {
            return $query->whereHas('kategori', function ($query) use ($kategori) {
                $query->where('kategori_id', $kategori);
            });
        });

        // Tambahan logika untuk regencies_id
        $query->when($filters['regencies_id'] ?? false, function ($query, $regencies_id) {
            return $query->where('regencies_id', $regencies_id);
        });
    }

    protected $hidden = [];

    public function kategori()
    {
        // return $this->belongsTo(Kategori::class, 'kategori_id', 'id'); //codingan asli
        return $this->belongsTo(Kategori::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'provinces_id', 'id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regencies_id', 'id');
    }

    public function gambars()
    {
        return $this->hasMany(Gambar::class, 'product_id', 'id');
    }
}
