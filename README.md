<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## CMS FLORIST (SMART FLORIST)

Nama Anggota :

1. Aldet Kirana Satria
2. Krisna Jorgi Hutahuruk
3. Sani Putri Sinaga
4. Zoya Simbolon

## SMART FLORIST

SmartFlorist adalah platform distributor bunga dan toko bunga online yang menghubungkan toko bunga dari berbagai wilayah di Indonesia dengan pembeli. Dengan beragam pilihan karangan bunga, Smart Florist bertujuan menjadi tempat terpercaya untuk memasarkan berbagai karangan bunga dari berbagai toko di Indonesia. Dengan situs web ini, pelanggan tidak hanya dapat dengan mudah menemukan karangan bunga impian mereka, tetapi juga dapat memasarkan dan mengiklankan kreasi karangan bunga mereka sendiri.

## Fitur Utama

Tampilan User
Header : Berisi navbar pulau yang didalamnya ada provinsi dan kabupaten yang mengarahkan user ke halaman product sesuai kabupaten yang dipilih
Slider : Berisi Slider yang menampilkan gambar, judul slider, deskripsi slider, dan nomor whatsapp
Halaman Produk Best Seller : Berisi Produk Produk Best Seller
Daftar Kategori : Berisi Kategori Produk yang mengarahkan user ke halaman product sesuai kategori yang dipilih
Halaman About Us : Berisi Keterangan tentang website dan gambar konten
Footer : Berisi About Us, Contact, dan Sosial Media Website

Tampilan Admin
Login : Admin dapat login dengan menggunakan email dan password yang sudah disiapkan
Dashboard : Berisi Informasi jumlah dari beberapa data
Sidebar : Berisi CRUD untuk Data Produk, Data Kategori, Data Slider, Data Konten, Data Lainnya ( Data About Us, Data Contact, Data Sosmed, Data Footer)
Data Produk : Berisi Data Data Produk, Admin dapat menambah , mengedit , dan menghapus produk.
Data Kategori : Berisi Data Data Kategori , Admin Dapat menambah, mengedit, dan menghapus kategori.
Data Slider : Berisi Data Data Slider, Admin dapat menambah, mengedit, dan menghapus slider
Data Konten : Berisi Data Data Konten , Admin dapat mengedit halaman konten
Data About Us : Berisi Data About Us, Admin dapat mengedit halaman About Us
Data Contact : Berisi Data Contact untuk footer, admin dapat mengedit data contact
Data Sosmed : Berisi Data Sosmed untuk footer, admin dapat mengedit data sosmed
Data Footer : admin dapat mengedit footer
Logout : Admin Logout dan mengarah ke halaman user

### Requirement

PHP 8.2.4
Laravel 10
MySQL
Bootstrap 4.5

## Installation

1. Git clone https://gitlab.com/kelompok-florist/project-smart-florist.git
2. cd project-smart-florist
3. composer update
4. cp .env.example .env
5. atur env database sesuai kebutuhan sistem
6. jalankan database lalu php artisan migrate
7. php artisan db:seed --class=UsersTableSeeder (seeder untuk login admin)
8. php artisan db:seed --class=AllSeeder (seeder untuk data dummy)
9. php artisan key:generate ( untuk meng-generate key aplikasi)
10. php artisan serve

## Link ERD

https://drive.google.com/drive/folders/1C5v9QbD9mE-KKBANzI0NlW243KBIedY8

## Link TRD

https://docs.google.com/document/d/1XQlehDGD6UfsbOAVdTffQYKoRM-dQZMa/edit#heading=h.gjdgxs
